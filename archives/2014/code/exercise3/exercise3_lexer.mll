{
  open Lexing
  open Exercise3_parser

  (** Increments the lexing buffer line number counter.*)
  let increment_linenum lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      {pos with pos_lnum = pos.pos_lnum + 1; pos_bol = 0}
  
  (** Increments the lexing buffer line offset by the given length. *)
  let increment_bol lexbuf length =
    let position = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <- {position with pos_bol = position.pos_bol + length}

  (** Prints a lexing error for an unrecognized character. *)
  let unrecognized_char position character =
    Printf.printf "Error line %d character %d: Unrecognized character '%c'\n"
                  position.pos_lnum (position.pos_bol + 1) character
}

let blank = [' ' '\t' '\r']
let digit = ['0'-'9']
let number = digit+('.'digit+)?

rule main = parse
  | "//" {comment lexbuf}
 
  | '\n'  {increment_linenum lexbuf; main lexbuf}
  | blank {increment_bol lexbuf 1; main lexbuf} 

  | number as x   {increment_bol lexbuf (String.length x); VAL(float_of_string x)}

  | '(' {increment_bol lexbuf 1; BEGIN_PAR}
  | ')' {increment_bol lexbuf 1; END_PAR}

  | '+'  {increment_bol lexbuf 1; PLUS}
  | '-'  {increment_bol lexbuf 1; MINUS}
  | '*'  {increment_bol lexbuf 1; MUL}
  | '/'  {increment_bol lexbuf 1; DIV}

  | eof {EOF}
  | _   as x {unrecognized_char lexbuf.lex_curr_p x; increment_bol lexbuf 1; main lexbuf}
  

and comment = parse
  | '\n' {increment_linenum lexbuf; main lexbuf}
  | eof  {EOF}
  | _    {comment lexbuf}

{}

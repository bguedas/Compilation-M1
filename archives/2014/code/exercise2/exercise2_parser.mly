%{
  (** A map to store the variables associated with their value. *)
  let variables = Hashtbl.create 16

  (**
    * Gets the value of the given variable from the map. Returns 0 if not found.
    *)
  let get_variable_value variable =
    try
      Hashtbl.find variables variable
    with Not_found ->
      begin 
        Error.not_initialized variable (symbol_start_pos ());
        0.
      end

  (**
    * Evaluates the given opeation with the given operands and stores the result
    * in the map for the given variable.
    *)
  let evaluate_operation var operand1 operation operand2 =
    let result = operation operand1 operand2 in
    if Hashtbl.mem variables var
      then
        begin
          Hashtbl.replace variables var result;
          Error.already_initialized var (symbol_start_pos ());
          result;
        end
      else
        begin
          Hashtbl.add variables var result;
          result
        end
%}

%token EOF
%token PLUS MINUS MUL DIV AFF
%token <string> VAR
%token <float> VAL

%start main
%type <unit> main

%%

main:
  expressions EOF {}
;

expressions:
    expression {print_endline (string_of_float $1)}
  | expression expressions {}
;

expression:
    VAR AFF operand operation operand {evaluate_operation $1 $3 $4 $5}
;

operand:
    VAR {get_variable_value $1}
  | VAL {$1}

operation:
    PLUS  {( +. )}
  | MINUS {( -. )}
  | MUL   {( *. )}
  | DIV   {( /. )} 
;

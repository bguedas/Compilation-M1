\documentclass[TP,sansRappel]{tdtp}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{url}
\usepackage{graphicx}
\usepackage{graphics}

\lstset{tabsize=3, basicstyle=\footnotesize, commentstyle=\itshape, stringstyle=\ttfamily, language=C++}

\newcommand{\encadre}[1]{\begin{center}\begin{tabular}{|c|}\hline #1 \\ \hline\end{tabular}\end{center}}
\newcommand{\mR}{\mathbb{R}}
\newcommand{\mN}{\mathbb{N}}
\def\euro{\mbox{\raisebox{.25ex}{{\it =}}\hspace{-.5em}{\sf C}}}

\title{Projet : Langage de génération d'images SVG}
\cursus{M1 ALMA-ORO}
\date{13 frevrier 2014}
\intitule{Compilation}

\begin{document}

%---------------------------------------------------------------------
\begin{feuille}[5]

%===============

\section{Objectif}

\noindent Le but est de générer des images au format SVG~\cite{wikipedia} à partir d’un langage de
programmation décrivant le dessin. Vous devez écrire le compilateur en Ocaml avec ocamllex et 
ocamllyacc pour le langage décrit ci-dessous.

\section{SVG}

\begin{figure}[htp]
        \centering
	\includegraphics[scale=0.4]{exemplesvg.png}
	\caption{Exemple de figure SVG}
	\label{exemplesvg}
\end{figure}

\noindent SVG est un format XML de description d’image. Il s’agit d’un format dit vectoriel. La page 
Wikipedia sur SVG~\cite{wikipedia} donne comme exemple le dessin de la Figure~\ref{exemplesvg}. Le 
code SVG de ce dessin est le suivant~:

\begin{verbatim}
<?xml version="1.0" encoding="utf-8"?>
  <svg
    xmlns="http://www.w3.org/2000/svg"
    version="1.1"
    width="300"
    height="200">
  <title>Exemple simple de figure SVG</title>
  <desc>
    Cette figure est constituée d’un rectangle,
    d’un segment de droite et d’un cercle.
  </desc>
  <rect
    width="100" height="80"
    x="0" y="70"
    fill="green" />
  <line
    x1="5" y1="5"
    x2="250" y2="95"
    stroke="red" />
  <circle
    cx="90" cy="80"
    r="50"
    fill="blue" />
  <text x="180" y="60">
    Un texte
  </text>
</svg>
\end{verbatim}

\noindent Il existe de nombreux sites internet sur lesquels vous pouvez trouver beaucoup 
d’informations utiles sur le SVG, par exemple~\cite{svg} pour une description des formes 
élémentaires, et \cite{norme}  pour une définition de la norme SVG 1.1.

\section{Langage}

\noindent Dans cette section, nous décrivons les principaux éléments du langage à implémenter. Cette
description n'est cependant pas exhaustive.

\subsection{Types}

\subsubsection{Types primitifs}

\noindent Les types primitifs sont les entiers, les flottants, les booléens, les chaînes de 
caractères et les couleurs. Ils sont définis de la façon suivante~:
\begin{itemize}
  \item les entiers sont composés d'une suite de chiffres allant de 0 à 9~;
  \item les flottants sont composés d'une suite d'entiers suivie d'un point et d'une suite d'entiers
        (minimum un entier de chaque c\^oté du point)~;
  \item les cha\^ines de caractères sont composées de toute suite de caractères entre guillemets 
        droits ('\texttt{"}')~;
  \item les booléens sont \texttt{true} ou \texttt{false}~;
  \item les couleurs sont \texttt{red}, \texttt{blue}, \texttt{yellow}, \texttt{green}, 
        \texttt{black} et \texttt{white}.
\end{itemize}

\subsubsection{Opérations sur les types primitifs}

\noindent Le langage permet des opérations arithmétiques entre nombres entiers et entre nombres
flottants. On ne peut cependant pas mélanger entiers et flottants dans une opération~: une opération 
entre entiers est de type entier, et une opération entre flottants est de type flottant. Les 
opérations arithmétiques à implémenter sont l'addition,la soustraction, la multiplication, la 
division et le modulo (\texttt{\%}).\\

\noindent Les opérateurs de comparaison usuels (\texttt{<}, \texttt{>}, \texttt{<=} et 
\texttt{>=}) doivent \^etre implémentés entre entiers et entre flottants. Les expressions à valeur 
booléennes peuvent être composées avec les opérateurs booléens \texttt{and} et \texttt{or}.\\

\noindent Les opérations doivent pouvoir comporter des parenthèses.

\subsubsection{Types complexes}

\noindent À ces types primitifs, s'ajoutent des types complexes. Les types complexes sont créés par 
des constructeurs prenant des paramètres entre parenthèses. Les valeurs sont ensuite accessibles via 
des attributs (en utilisant la notation pointée)~:
\begin{itemize}
  \item \texttt{Circle}~: prend deux flottants pour les coordonnées de son centre (\texttt{x} et 
        \texttt{y}), et un flottant pour le rayon (\texttt{radius})~;
  \item \texttt{Rectangle}~: prend deux flottants pour son point d'extrémité 
        haute/gauche (\texttt{x} et \texttt{y}), et deux flottants pour respectivement sa largeur 
        (\texttt{width}) et 	sa hauteur (\texttt{height})~;
  \item \texttt{Line}~: prend deux flottants pour son origine (\texttt{x1} et \texttt{y1}) et deux 
        flottants pour sa destination (\texttt{x2} et \texttt{y2})~;
  \item \texttt{Text}~: prend en paramètre deux flottants pour sa position (\texttt{x} et 
        \texttt{y}), et une chaîne de caractère pour son contenu (\texttt{content})~;
  \item \texttt{Image}~: prend deux entiers pour la largeur (\texttt{width}) et la hauteur 
        (\texttt{height}).\\
\end{itemize}

\noindent Les types complexes décrivant des formes comportent aussi des champs optionnels pour 
définir leur couleur ou leur épaisseur (quand cela peu s'appliquer). Par défaut, nous considérons 
que l'épaisseur est de 1, la couleur de contour est le noir, et la couleur de remplissage est le blanc. La couleur de contour est définie par l'attribut \texttt{stroke}, la couleur de remplissage est définie par l'attribut \texttt{fill}, et l'épaisseur du contour est définie par l'attribut
\texttt{stroke-width}.

\noindent Pour les éléments de type texte, on peut définir leur couleur (\texttt{color}) et la 
taille de leur fonte (\texttt{font-size}). Par défaut, le texte est en noir, de taille 12.

\begin{verbatim}
  texte := Text("un texte");
  text.color := blue;
  text.font-size := 10;
\end{verbatim}

\subsection{Variables et instructions}

\noindent Les instructions sont séparées par des points-virgules. Les types d'instruction sont
\begin{itemize}
  \item les déclaration / affectation~;
  \item la primitive de dessin~;
  \item les structures de contr\^ole.\\
\end{itemize}

\noindent Les valeurs de type primitif ou de type complexe peuvent être affectées à des variables 
dont le nom est composé de lettres (majuscules ou minuscules), de chiffres et du caractère '\_'. 
Un nom de variable doit obligatoirement commencer par une lettre.

La déclaration d'une variable se fait simplement à sa première affectation. L'affectation se fait à 
l'aide de l'opérateur d'affectation '\texttt{:=}'. 

Les valeurs qui composent les types complexes peuvent être lues et modifiées (seuls les attributs 
des images ne peuvent être modifiés).\\

\noindent Voici des exemples de déclaration / affectation de variables~:

\begin{verbatim}
  une_chaine := "le contenu de la chaine";
  x := 0.1;
  deux := 2;
  rouge := red;
  b := true;
  cercle := Circle(0.0, 0.0, 2.0);
  cercle.radius := 3.0;
  rect := Rectangle(0.0, 0.0, 4.0, 5.0);
  texte := Text(une_chaine);
  text.color := rouge;
  image := Image(1024, 768); 
\end{verbatim}

\noindent La primitive de dessin \texttt{draw} prend en paramètre une image et un type de forme 
(\texttt{Point}, \texttt{Circle}, \texttt{Rectangle}, \texttt{Line}), et ajoute la forme à 
l'image. Pour chaque nouvel élément de type image passé à une primitive \texttt{draw}, un fichier 
SVG est créé avec le nom de la variable.

Par exemple, soit une deux images i1 et i2, deux cercles c1 et c2, et un rectangle r, les 
instructions suivantes créent deux images \emph{i1.svg} et \emph{i2.svg} contenant respectivement 
c1, r et c2~: 

\begin{verbatim}
  draw i1 c1;
  draw i1 r;
  draw i2 c2;
\end{verbatim}

\noindent Les structures de contr\^ ole à implémenter sont le branchement~:

\begin{verbatim}
  if (<booléen>)
    then {
      <instructions>
    } [else {
      <instruction>
    }]
\end{verbatim}

\noindent et la boucle tant que~:

\begin{verbatim}
  while (<booléen>) {
    <instructions>
  }
\end{verbatim}

\noindent Ces deux structures sont identiques à celles de C ou de Java, à part qu'elles délimitent 
forcément les instructions par des accolades.

\subsection{Commentaires}

\noindent Des commentaires peuvent \^etre inclus dans le code à n'importe quel endroit. Ces
commentaires sont semblables à ceux de Python~: tous les caractères situés après le caractère
'\#' sont ignorés jusqu'à la fin de la ligne.

\subsection{Erreurs}

\noindent Votre compilateur doit relever les erreurs suivantes~:
\begin{itemize}
  \item variable non déclarée~;
  \item motif non reconnu~;
  \item éléments graphiques incohérents (par exemple~: longueurs négatives)\\
\end{itemize}

\noindent Pour chaque erreur, il vous appartient de décider si elle interrompt la compilation ou 
non. Vos choix doivent être justifiés dans le rapport.

\subsection{Exemple}

\noindent Voici un exemple de code générant une image SVG~:

\begin{verbatim}
x := 0.6;
center := Point(x, 0.0);
radius := 6.2;

circle := Circle(center,radius);
circle.background := blue;
circle.border := red;

image := Image(800, 600);

i := 0;
while (i < 10) {
  circle.radius := circle.radius + i;
  if (i%2 = 0)
    then {
      # dessine le cercle c dans l'image image.svg
      draw image c;
    } else {
      circle.center := circle.center
    }
  i := i + 1;
}
\end{verbatim}

\section{Instructions}

\noindent Ce projet est à réaliser seul. Vous devez rendre une archive au format .tar.gz ou .zip par 
courriel au plus tard une semaine après la dernière séance de TP. Cette archive doit contenir les 
sources de votre programme, un Makefile pour le compiler ainsi qu’un rapport de TP au format PDF. 
N'incluez pas d'exécutable. Votre archive doit être nommée prénom\_nom.tar.gz où. La décompression 
de l’archive doit créer un dossier du même nom contenant tous les éléments.

Vous n’êtes pas obligés d’implémenter la totalité du langage, mais il vous est demandé 
d’en faire le plus possible, et de rendre un programme qui fonctionne. N’hésitez pas non plus à 
ajouter des fonctionnalités si vous le pouvez (mais seulement si vous avez implémenté toutes celles 
décrites).

Votre compilateur doit être réalisé de façon incrémentale (ayez une étape qui fonctionne
avant de passer à la suivante) dans l'ordre de votre choix. Par exemple, il ne devra pas 
implémenter les boucles si les objets de base ne fonctionnent pas. Cela vous permettra
de progresser graduellement en complexité. Il est conseillé de conserver une copie 
du code à chaque étape. Vous pourrez alors inclure les différentes étapes dans l'archive que vous 
rendrez et les décrire dans le rapport en expliquant vos choix. Si certaines instructions vous 
semblent vagues, justifiez votre interprétation dans la rapport.

Le rapport est la partie la plus importante du projet et ne doit donc pas être négligée au profit du 
code. Le rapport ne doit pas simplement décrire votre implémentation, mais plutôt votre démarche et
vos choix de conception. Vous devez décrire le fonctionnement de votre analyseur lexical et 
syntaxique, la grammaire du langage que vous avez créé et les choix que vous avez faits, les 
structures de données que vous avez utilisées et les algorithmes implémentés pour les parcourir.

N’hésitez pas à demander de l’aide à l’enseignant de TP pour l’implémentation en Ocaml ou toute 
autre indication. Des indications et exemples de code Ocaml seront donnés lors des séances.

\begin{thebibliography}{1}
\bibitem[1]{wikipedia} Page SVG de Wikipedia~: \url{http://fr.wikipedia.org/wiki/Svg}
\bibitem[2]{svg} Cours sur le SVG ~: 
\url{http://www.liafa.jussieu.fr/~carton/Enseignement/XML/Cours/SVG/index.html}
\bibitem[3]{norme} La norme SVG 1.1~: \url{http://www.w3.org/TR/SVG11/}
\end{thebibliography}

\end{feuille}

\end{document}

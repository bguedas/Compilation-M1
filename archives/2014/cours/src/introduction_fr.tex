\documentclass[TP,sansRappel]{tdtp}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{url}

\lstset{tabsize=3, basicstyle=\footnotesize, commentstyle=\itshape, stringstyle=\ttfamily, language=C++}

\newcommand{\encadre}[1]{\begin{center}\begin{tabular}{|c|}\hline #1 \\ \hline\end{tabular}\end{center}}
\newcommand{\mR}{\mathbb{R}}
\newcommand{\mN}{\mathbb{N}}
\def\euro{\mbox{\raisebox{.25ex}{{\it =}}\hspace{-.5em}{\sf C}}}

\title{Introduction \`a Lex et Yacc en Ocaml}
\cursus{M1 ALMA-ORO}
\date{16 janvier 2014}
\intitule{Compilation}

\begin{document}

%---------------------------------------------------------------------
\begin{feuille}[1]

%===============

\section{Introduction}

Lex et Yacc sont des outils de g\'en\'eration d'analyseurs lexicaux et 
syntaxiques. Leurs impl\'ementations GNU se nomment respectivement Flex \cite
{flex} et Bison \cite{bison}. \`A partir d'une grammaire qui leur est propre, ces 
outils vont g\'en\'erer des analyseurs en langage C. Dans ce TP, nous 
n'utiliserons pas Flex et Bison, mais leur impl\'ementation en Objective-Caml 
(Ocaml) qui se nomment respectivement ocamllex et ocamlyacc \cite{doclexyacc}.

Quelques exemples simples de Lex et Yacc en Ocaml peuvent \^etre 
trouv\'es sur internet \cite{exempleslexyacc}. Mais la syntaxe des fichiers Lex
et Yacc \'etant identique \`a la version C (\`a part les actions \`a 
r\'ealiser et le code autour), vous pourrez \'egalement vous inspirer d'exemples en C.\\

Attention, cette introduction n'a pas pour ambition d'être exhaustive sur les 
possibilités offertes par ces outils, mais simplement vous fournir les briques
de bases pour démarrer.

\subsection{Lex}

Lex sert \`a g\'en\'erer des analyseurs lexicaux. Un analyseur lexical va 
d\'eclancher des actions quand des motifs sont reconnus dans le flux entrant. 
Le but de l'analyse lexicale est de lire des suite de caract\`eres et de les 
reconna\^itre comme des mots cl\'es, des variables, des nombres, etc. Les 
motifs sont repr\'esent\'es par des expressions rationnelles (souvent mal traduites de l'anglais en \emph{expressions r\'eguli\`eres} - \emph{regexp}).

Les fichiers ocamllex portent l'extension \emph{.mll} et sont organis\'es en
trois parties de la manière suivante :

\begin{VerbProgram}
  { <Entête> }
  <Motifs, règles et actions associées>
  { <Sous-programmes> }
\end{VerbProgram}

Les parties \emph{<Entête>} et \emph{<Sous-programmes>} vont contenir du code
Ocaml. Généralement la partie \emph{<Entête>} contient les déclarations 
d'inclusion \emph{Open} et des fonctions de traîtement auxiliaires. La partie
\emph{<Sous-programmes>} peut contenir des fonctions ou le programme principal
si besoin. Ces champs ne sont pas obligatoires, mais basiquement, le premier
contient des déclarations devant être exécutées au début de l'analyse et le 
second à la fin.

La partie centrale, \emph{<Motifs, règles et actions associées>} est la plus
importante et constitue le c\oe{}ur de l'analyseur. Chaque motif à reconnaître
est associé à un ensemble d'actions :

\begin{VerbProgram}
   <Motif> {<Action>}
\end{VerbProgram}

Le motif \emph{<Motif>} peut être simplement un chaîne de caractère (écrite 
entre guillemets en Ocaml) ou un motif ayant préalablement été déclaré. 
L'action est une fonction ou une suite de fonctions Ocaml, ou un mot clé 
(token) à envoyer à l'analyseur syntaxique. Il existe certains motifs spéciaux
tels que \emph{eof} qui reconnaît la fin de fichier et \emph{\_} qui reconnaît
n'importe quoi d'autre.

Les règles peuvent être regroupées de la manière suivante :

\begin{VerbProgram}
  rule <règles1> <arguments> = parse 
     <Motif> { <Action> }
   | …
   | <Motif> { <Action> }
  and <règles2> <arguments> = parse …
  and …
\end{VerbProgram}

Les motifs sont formés par des expressions rationnelles à la manière des outils
usuels UNIX tels que \emph{grep}. Vous reporter à la documentation 
\cite{doclexyacc} et aux exemples pour plus de précisions. Les motifs sont
déclarés à l'aide du mot clé \emph{let} qui sert en Ocaml à la déclaration de
variables et de fonctions. Un motif déclaré peut être réutilisé dans 
l'expression d'un autre motif ou d'une règle. Par exemple :

\begin{VerbProgram}
  let entier = [0-9]+
  let flottant = entier'.'entier
  
  rule token = parse
     entier          {print_endline("Entier")}   (* impression de Entier à l'écran *)
   | flottant as lxm {NUM(float_of_string lxm)}  (* envoi du mot-clé typé NUM *)
   | _               {TRUC}                      (* envoi du mot-clé TRUC *)
\end{VerbProgram}

Notez que les commentaires sont écrits en Ocaml entre (* et *). On peut 
également utiliser des alias pour les motifs reconnus afin de les utiliser
dans les actions (ici \emph{lxm}). Les mots clés sont envoyés à l'analyseur
syntaxique.

\subsection{Yacc}

YACC (Yet Another Compiler Compiler) g\'en\`ere des analyseurs syntaxiques. Un 
analyseur syntaxique va d\'eclancher des actions quand des structures 
grammaticales sont reconnues.

Les fichiers ocamlyacc portent l'extension \emph{.mly} et sont organisés en 
quatres parties :

\begin{VerbProgram}
  %{
    <Entête>
  %}
    <Déclarations>
  %%
    <Règles>
  %%
    <Sous-programmes>
\end{VerbProgram}

Comme pour ocamllex, les parties \emph{<Entête>} et \emph{<Sous-programmes>}
contiennent du code Ocaml.\\

La partie \emph{<Déclarations>} contient la déclaration des mots reconnus par
la grammaire et qui sont principalement envoyés par l'analyseur lexical. Ces
déclaration sont précédées de \emph{\%token} et peuvent être écrites sur une
seule ligne ou sur plusieurs :

\begin{VerbProgram}
  %token <Liste de mots-clés>
  %token <Liste de mots-clés>
\end{VerbProgram}

Par exemple, en reprenant le dernier exemple donné à la section précédente sur 
Lex, nous avons deux mots-clés (NUM et TRUC) à déclarer :

\begin{VerbProgram}
  %token <float>NUM
  %token TRUC
\end{VerbProgram}

Le \emph{<float>} collé avant NUM indique de NUM est de type \emph{float}. Il
peut s'agir de tout autre type Ocaml.

Dans cette section, il faut également déclarer par quelle règle la grammaire
commence à l'aide de \emph{\%start} et quel est sont type. Par exemple si notre
première règle s'appelle \emph{main} et que la structure renvoyée par 
l'analyseur est de type \emph{Arbre}, on aura la déclaration suivante :

\begin{VerbProgram}
  %start main
  %type <Arbre>main
\end{VerbProgram}

La partie \emph{<Règles>} contient la grammaire qui est décrite sous une 
forme proche de BNF \footnote{Backus–Naur Form, grammaire décrite à l’aide de règles de dérivations de telle facon que les symboles n’apparaissant pas dans la partie gauche de la règle sont les symboles dits terminaux.}. Les règles
syntaxiques sont écrites sous la forme suivante :

\begin{VerbProgram}
<Non-terminal> :
    <Symbole> … <Symbole> { <Action sémantique> }
  | …
  | <Symbole> … <Symbole> { <Action sémantique> }
;
\end{VerbProgram}

Comme son nom l'indique \emph{<Non-terminal>} représente un symbole non-
terminal de la grammaire qui constituera ici le nom des règles. \emph{Symbole}
désigne un symbole terminal ou non-terminal. Un symbole termial sera ici un
caractère ou un mot-clé déclaré précédemment.

Les actions sémantiques sont des parties de codes Ocaml. Elles utilisent les
variables \emph{\$1}, \emph{\$2}, etc. pour représenter les
valeurs prises par les symboles. \emph{\$1} est par exemple la valeur du 
premier symbole.\\

La section \emph{exemples} de ce document présente des exemples complets.

\subsection{Ocaml}

Le TP devra être réalisé en Ocaml avec ocamllex et ocamlyacc. Cependant, 
le but du TP n'étant d'aprendre le langage Ocaml mais Lex et Yacc, les parties de code 
n\'ecessaires en Ocaml seront donn\'ees. Sinon, n'h\'esitez pas \`a vous 
documenter par vous-m\^eme et poser des questions durant les TPs.

La documentation officielle d'Ocaml est disponible sur internet \cite{dococaml}
ainsi que des ouvrage complets librement consultables \cite{livreocaml,realworldocaml}.

\section{Exemples}

Les exemples sont disponibles sur GitHub~: \url{https://github.com/bguedas/Compilation-M1-ALMA}.

\subsection{Exemple 1}

Le premier exemple se trouvant dans le répertoire \emph{example1} ne contient
qu'un seul fichier ocamllex. Vous pouvez le compiler avec ligne de commande
suivante :

\begin{verbatim}
  shell# ocamllex example1_lexer.mll
\end{verbatim}

Vous verrez alors qu'un analyseur lexical en Ocaml a été généré~: 
\emph{example1\_lexer.ml}. Vous pouvez compiler cet analyseur comme un fichier 
Ocaml classique :

\begin{verbatim}
  shell# ocamlc example1_lexer.ml -o example1
\end{verbatim}

Vous avez maintenant un exécutable sur lequel vous pouvez essayer le fichier
de test \emph{example1\_test.txt} :

\begin{verbatim}
  shell# ./example1 example1_test.txt
\end{verbatim}

\subsection{Exemple 2}

Le second exemple est plus complet et contient un fichier ocamllex, un fichier
ocamlyacc et des fichiers de code Ocaml. Il vous montre comment interfacer ces
différents fichiers et comment les compiler. Pour la compilation, un makefile
est fourni. La commande suivante compilera l'exemple~:

\begin{verbatim}
  shell# make
\end{verbatim}

Vous pouvez étudier les fichiers créés. L'exécutable créé se nomme 
\emph{example2} et vous pouvez l'essayer sur le fichier d'exemple 
\emph{example2\_test.txt} :

\begin{verbatim}
  shell# ./example2 example2_test.txt
\end{verbatim}

Pour effacer tous les fichiers créés à la compilation à l'exception de l'exécutable, utilisez la commande~:

\begin{verbatim}
  shell# make clean
\end{verbatim}

\subsection{Exemple 3}

Le troisième exemple se compile comme l'exemple~2~:

\begin{verbatim}
  shell# make
\end{verbatim}

L'exécution du programme affiche sur la sortie standard le contenu d'un fichier \emph{dot}.
La commande \emph{dot} est un module du logiciel de tracé de graphes \emph{Graphviz}~\cite{graphviz,exdot}.\\
Vous pouvez rediriger la sortie dans un fichier :

\begin{verbatim}
 shell# ./example3 example3_test.txt > example3_test.dot
\end{verbatim}

Le fichier \emph{dot} produit peut ensuite être transformé en image~:

\begin{verbatim}
 shell# dot exemple3_test.dot -Tpng -o example3_test.png
\end{verbatim}


\section{Travail à effectuer}

\begin{itemize}
  \item Récupérer les fichiers ;
  \item Exécuter les programmes ;
  \item Comprendre le fonctionnement des exemples ;
  \item Créer d'autres fichiers d'entrée ;
  \item Modifier les programmes pour ajouter des fonctionnalités et étudier leur comportement.
\end{itemize}

\begin{thebibliography}{1}
\bibitem[1]{flex} Site officiel de Flex : \url{http://flex.sourceforge.net/}
\bibitem[2]{bison} Site officiel de Bison : \url{http://www.gnu.org/software/bison/}
\bibitem[3]{doclexyacc} Lex et Yacc dans la doc officielle Ocaml : \url{http://caml.inria.fr/pub/docs/manual-ocaml-4.00/manual026.html}
\bibitem[4]{exempleslexyacc} Exemples simples de Lex et Yacc avec Ocaml : \url{http://www.matt-mcdonnell.com/code/code_ocaml/lexing/lexing.html}
\bibitem[5]{dococaml} Documentation officielle d'Ocaml : \url{http://caml.inria.fr/pub/docs/manual-ocaml/index.html}
\bibitem[6]{livreocaml} Livre ancien, en anglais sur Ocaml : \url{http://caml.inria.fr/pub/docs/oreilly-book/index.html}
\bibitem[7]{realworldocaml} Livre récent, en anglais sur Ocaml : \url{https://realworldocaml.org/v1/en/html/index.html}
\bibitem[8]{graphviz} Site officiel de graphviz : \url{http://www.graphviz.org/}
\bibitem[9]{exdot} Gallerie d'exemples Graphviz : \url{http://www.graphviz.org/Gallery.php}
\end{thebibliography}

\end{feuille}

\end{document}

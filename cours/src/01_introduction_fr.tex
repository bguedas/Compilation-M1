\documentclass[TP,sansRappel]{tdtp}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{url}

\lstset{tabsize=3, basicstyle=\footnotesize, commentstyle=\itshape, stringstyle=\ttfamily, language=C++}

\newcommand{\encadre}[1]{\begin{center}\begin{tabular}{|c|}\hline #1 \\ \hline\end{tabular}\end{center}}
\newcommand{\mR}{\mathbb{R}}
\newcommand{\mN}{\mathbb{N}}
\def\euro{\mbox{\raisebox{.25ex}{{\it =}}\hspace{-.5em}{\sf C}}}

\title{Introduction}
\cursus{M1 ALMA-ORO}
\date{15 janvier 2015}
\intitule{Compilation}

\begin{document}

%---------------------------------------------------------------------
\begin{feuille}[1]

%===============

\section{Introduction}

\noindent La compilation consiste à transformer un langage source en un 
langage cible. On pense en particulier aux compilateurs ciblant un 
langage machine pour produire un binaire directement exécutable. Il 
existe cependant de plus en plus de compilateurs ayant pour cible un 
autre langage de programmation tel que le \emph{C} ou le
\emph{JavaScript}, le \emph{JavaScript} étant souvent considéré comme 
l'assembleur du \textit{web} (\textit{Cf.} le tableau~\ref{ex_compilateurs}). 
Beaucoup de compilateurs ciblent également une 
machine virtuelle. Parmi les plus connus on retrouve la machine 
virtuelle Java (Java, Scala, Groovy, Ceylon, Kotlin, Clojure\dots), et 
la machine virtuelle CLR pour .Net (C\#, F\#, VB.NET, PowerShell, Clojure\dots).

\begin{table}[h]
   \centering
   \begin{tabular}{|l|l|l|}
       \hline
       \textbf{Compilateur} & \textbf{Langage source} & \textbf{Langage cible} \\
       \hline
       CoffeeScript & CoffeeScript & JavaScipt \\
       dart2js & Dart & JavaScript \\
       FalconJX & MXML \& ActionScript & JavaScript \\
       js\_of\_ocaml & Ocaml & JavaScript \\
       valac & Vala & C \\
       emscripten & code octet LLVM & JavaSCript \\
       Cython & Python & C \\
       \hline
   \end{tabular}
   \caption{\label{ex_compilateurs} Exemples de compilateurs ciblant un autre langage de programmation.}
\end{table}

\noindent L'intérêt de compiler dans un autre langage ou pour une 
machine virtuelle est de  s'abstraire de la multitude de cibles 
possibles (différents systèmes, différentes architectures  processeurs) 
en n'en ciblant qu'une seule. C'est ensuite au compilateur du langage 
cible ou à la machine virtuelle de faire la traduction. Le code ainsi 
produit peut tirer partie de l'environnement du langage cible (environnement d'exécution, bibliohèques\ldots).\\

\noindent Un compilateur ne se limite pas seulement à traduction d'un 
langage dans un autre, mais doit aussi rapporter les erreurs existantes 
dans la fichier source. La figure~\ref{fig:compilateur} montre une vue 
très schématique d'un compilateur.\\

\begin{figure}[h]
\begin{center}
\begin{tikzpicture} 
\draw  (0,0) rectangle (3,1.8) 
	(1.5,0.9) node {Compilateur}; 
\draw[->] (-1,0.9) node[left] {programme source} --(0,0.9);
\draw[->] (3,0.9)--(4,0.9) node[right] {programme cible};
\draw[->] (1.5,0) -- (1.5,-1) node[below] {messages d'erreur};
\end{tikzpicture} 
\caption{\label{fig:compilateur} Un compilateur, d'après~\cite{dragon}.}
\end{center}
\end{figure}

\noindent Ce TP n'a pas pour but d'être un cours complet de 
compilation~; pour cela, se référer au cours ou aux ouvrages de 
référence~\cite{dragon}. L'objectif est de présenter des outils aidant à
la réalisation de parties importantes d'un compilateur que sont l'analyse
lexicale et l'analyse syntaxique.

\section{Les différentes parties d'un compilateur}

\subsection{L'environnement d'un compilateur}

\noindent Pour produire un programme exécutable à partir d'un fichier 
source, un compilateur fait appel à plusieurs composants en amont et en 
aval du c\oe{}ur du compilateur à proprement parler. (voir
figure~\ref{fig:env_compilation}).

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}
\draw[->] (0,0) node[above] {programme source avec macros} --(0,-0.5);
\draw  (-1.5,-0.5) rectangle (1.5,-1) (0,-0.75) node {Préprocesseur};
\draw[->] (0,-1) --(0,-2) node[midway, right] {programme source};
\draw  (-1.5,-2) rectangle (1.5,-2.5) (0,-2.25) node {Compilateur};
\draw[->] (0,-2.5) --(0,-3.5) node[midway, right] {programme en assembleur};
\draw  (-1.5,-3.5) rectangle (1.5,-4) (0,-3.75) node {Assembleur};
\draw[->] (0,-4) --(0,-5) node[midway, right] {code machine relocalisable};
\draw[<-] (1.5,-5.25) --(2,-5.25) node[right] {bibliothèques, code machnine relocalisable};
\draw  (-1.5,-5) rectangle (1.5,-5.5) (0,-5.25) node {Éditeur de liens};
\draw[->] (0,-5.5) --(0,-6) node[below] {code machine};
\end{tikzpicture} 
\caption{\label{fig:env_compilation} Un compilateur complet produisant un exécutable.}
\end{center}
\end{figure}

\noindent Le programme source peut contenir des macros, telles que 
\texttt{\#define} ou \texttt{\#include} en \emph{C}. Ces macros définissent des 
traitements à réaliser sur le code source avant la compilation. Par exemple en 
\emph{C}, \texttt{\#include "fichier.h"} va copier le contenu du 
\textit{fichier.h} dans le fichier source où est définie la macro.

Le fichier source ainsi généré va pouvoir être compilé pour produire (par exemple) un fichier en langage assembleur pour l'architecture cible.

Le code assembleur produit peut ensuite être assemblé par un programme 
d'assemblage pour produire du code machine. Cette étape n'est pas de la 
compilation car la traduction de code assembleur en code machine est quasiment 
directe. L'assembleur apportant principalement des mnémoniques correspondant 
directement aux instructions machine.

Le code machine produit n'est pas directement exécutable, il doit encore être 
lié aux autres fichiers compilés et aux bibliothèques existantes. Les 
références et les adresses mémoires à utiliser sont mises à jour pour produire 
un programme exécutable.\\

\noindent Il est important d'avoir une vue générale des étapes permettant à 
partir d'un programme source d'obtenir un programme exécutable. Nous nous 
focaliserons, cependant, uniquement sur le c\oe{}ur du compilateur.

\subsection{Les différentes phases de compilation}

\noindent Le processus de compilation se décompose en deux phases principales~: 
une phase d'analyse et une phase de synthèse.

La phase d'analyse se décompose elle-même en trois phases~: l'analyse lexicale, 
l'analyse syntaxique et l'analyse sémantique. La phase de synthèse peut 
également se décomposer en trois phases~: la génération de code intermédiaire, 
l'optimisation de code et enfin, la génération de code (voir 
figure~\ref{fig:phases_compilation}). Durant tout le déroulement de ce 
processus,  le compilateur gère également la table des symboles et le 
traitement des erreurs.

L'analyse lexicale consiste à décomposer le flux d'entrée (le fichier source) en unités
lexicales ou \emph{lexèmes}. Un \emph{lexème} est une chaîne de caractères formant un
jeton (\textit{token} en anglais) ayant un rôle particulier dans le langage source. Les 
jetons sont ensuite passés à l'analyseur syntaxique qui va vérifier que leur  
organisation respecte la grammaire du langage source et construire un arbre syntaxique
représentant cette structure. L'analyseur sémantique va parcourir cet arbre, effectuer
des vérifications de sens (cohérence des types, de la portée des variables, 
\textit{etc}.), puis générer un code intermédiaire. Il s'agit d'un code dans un langage
simplifié à la manière de l'assembleur, mais adapté au langage source. Des optimisations
peuvent alors être réalisées sur ce code intermédiaire avant la génération du langage 
cible.

Ce découpage en différentes phases permet de faciliter la conception d'un 
compilateur en le décomposant en modules plus simples à implémenter. La 
distinction plus générale en phases d'analyse et de synthèse correspondent à ce
que l'on appelle, en anglais, le \textit{front-end} et le \textit{back-end}~:
le \textit{front-end} est lié au langage source, tandis que le
\textit{back-end} est lié au langage cible. Ainsi, en changeant seulement le
\textit{back-end} d'un compilateur, on peut cibler une autre architecture. Le
remplacement du \textit{front-end} est quant à lui plus délicat car le code
intermédiaire est souvent dépendant du langage source.\\  

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}
\draw[->] (0,0.5) node[left, align=center] {programme\\ source} -- (0.25,0.5);
\draw  (0.25,0) rectangle (1.75,1) (1,0.5) node[align=center] {Analyse\\ lexicale};
\draw[->] (1.75,0.5) -- (2,0.5);
\draw  (2,0) rectangle (3.75,1) (2.9,0.5) node [align=center] {Analyse\\ syntaxique};
\draw[->] (3.75,0.5) -- (4,0.5);
\draw  (4,0) rectangle (5.75,1) (4.9,0.5) node [align=center] {Analyse\\ sémantique};
\draw[->] (5.75,0.5) -- (6,0.5);
\draw  (6,-0.25) rectangle (8,1.25) (7,0.5) node [align=center] {Génération\\ de code\\ intermédiaire};
\draw[->] (8,0.5) -- (8.25,0.5);
\draw  (8.25,0) rectangle (10.25,1) (9.25,0.5) node [align=center] {Optimisation\\ de code};
\draw[->] (10.25,0.5) -- (10.5,0.5);
\draw  (10.5,0) rectangle (12.25,1) (11.4,0.5) node [align=center] {Génération\\ de code};
\draw[->] (12.25,0.5) -- (12.5,0.5) node[right, align=center] {programme\\ cible};
\end{tikzpicture} 
\caption{\label{fig:phases_compilation} Les différentes phases de compilation.}
\end{center}
\end{figure}

\noindent Dans ces séances de travaux pratiques, nous nous intéresserons 
principalement à l'analyse lexicale et à l'analyse syntaxique. L'écriture d'un 
analyseur lexical et d'un analyseur syntaxiques peut être grandement 
simplifiée par l'utilisation de générateurs d'analyseurs tels que \emph{Lex} et 
\emph{Yacc}.

\section{Outils utlisés}

\noindent \emph{Lex} et \emph{Yacc} sont des outils de g\'en\'eration d'analyseurs 
lexicaux et syntaxiques. Leurs impl\'ementations GNU se nomment respectivement 
\emph{Flex}~\cite{flex} et \emph{Bison} \cite{bison}. \`A partir d'une grammaire qui leur 
est propre, ces outils vont g\'en\'erer des analyseurs en langage \emph{C}. Dans ce TP, 
nous n'utiliserons pas \emph{Flex} et \emph{Bison}, mais leur impl\'ementation en 
\emph{Ocaml} qui se nomment respectivement \emph{ocamllex} et 
\emph{ocamlyacc}~\cite{doclexyacc}.

Quelques exemples simples de \emph{Lex} et \emph{Yacc} en \emph{Ocaml} peuvent \^etre 
trouv\'es sur internet \cite{exempleslexyacc}. Mais la syntaxe des fichiers \emph{Lex}
et \emph{Yacc} \'etant identique \`a la version \emph{C} (\`a part les actions \`a 
r\'ealiser et le code autour), vous pourrez \'egalement vous inspirer d'exemples en \emph{C}.\\

\noindent L'analyse lexicale et l'analyse syntaxique peuvent intervenir dès lors qu'un programme doit traiter un flux de chaîne de caractère 
structuré. Les outils et concepts présentés pourront donc vous être 
utiles dans d'autres domaines que la compilation.

Attention, cette introduction n'a pas pour ambition d'être exhaustive sur les possibilités offertes par ces outils, mais simplement vous fournir les briques de base pour démarrer.

\subsection{Lex}

\noindent \emph{Lex} sert \`a g\'en\'erer des analyseurs lexicaux. Un 
analyseur lexical va d\'eclancher des actions quand des motifs sont 
reconnus dans le flux entrant. Le but de l'analyse lexicale est de lire 
des suite de caract\`eres et de les reconna\^itre comme des mots-cl\'es, 
des variables, des nombres, etc. Les motifs sont repr\'esent\'es par des 
expressions rationnelles (souvent mal traduites de l'anglais en \emph
{expressions r\'eguli\`eres} - \textit{regexp}).

Les fichiers \emph{ocamllex} portent l'extension \texttt{.mll} et sont 
organis\'es en trois parties de la manière suivante~:

\begin{VerbProgram}
  { <Entête> }
  <Motifs, règles et actions associées>
  { <Sous-programmes> }
\end{VerbProgram}

\noindent Les parties \texttt{<Entête>} et \texttt{<Sous-programmes>} 
vont contenir du code \emph{Ocaml}. Généralement la partie \texttt{<Entête>} contient les déclarations d'inclusion \texttt{Open} et des 
fonctions de traîtement auxiliaires. La partie
\texttt{<Sous-programmes>} peut contenir des fonctions ou le programme 
principal si besoin. Ces champs ne sont pas obligatoires, mais 
basiquement, le premier contient des déclarations devant être exécutées 
au début de l'analyse et le second à la fin.

La partie centrale, \texttt{<Motifs, règles et actions associées>} est 
la plus importante et constitue le c\oe{}ur de l'analyseur. Chaque motif 
à reconnaître est associé à un ensemble d'actions~:

\begin{VerbProgram}
   <Motif> {<Action>}
\end{VerbProgram}

\noindent Le motif \texttt{<Motif>} peut être simplement une chaîne de 
caractères (écrite entre guillemets en \emph{Ocaml}) ou un motif ayant 
préalablement été déclaré. L'action est une fonction ou une suite de 
fonctions \emph{Ocaml}, ou un mot-clé (\textit{token}) à envoyer à 
l'analyseur syntaxique. Il existe certains motifs spéciaux tels que 
\texttt{eof} qui reconnaît la fin de fichier et \texttt{\_} qui reconnaît
n'importe quel caractère.\\

\noindent Les règles peuvent être regroupées de la manière suivante~:

\begin{VerbProgram}
  rule <règles1> <arguments> = parse 
     <Motif> { <Action> }
   | …
   | <Motif> { <Action> }
  and <règles2> <arguments> = parse …
  and …
\end{VerbProgram}

\noindent Les motifs sont formés par des expressions rationnelles à la 
manière des outils usuels \emph{UNIX} tels que \emph{grep}. Vous 
reporter à la documentation~\cite{doclexyacc} et aux exemples pour plus 
de précisions. Les motifs sont déclarés à l'aide du mot clé \texttt{let} 
qui sert en \emph{Ocaml} à la déclaration de variables et de fonctions. 
Un motif déclaré peut être réutilisé dans l'expression d'un autre motif 
ou d'une règle. Par exemple~:

\begin{VerbProgram}
  let entier = [0-9]+
  let flottant = entier'.'entier
  
  rule token = parse
     entier          {print_endline("Entier")}   (* impression de Entier à l'écran *)
   | flottant as lxm {NUM(float_of_string lxm)}  (* envoi du mot-clé typé NUM *)
   | _               {TRUC}                      (* envoi du mot-clé TRUC *)
\end{VerbProgram}

\noindent Notez que les commentaires sont écrits en \emph{Ocaml} entre 
\texttt{(*} et \texttt{*)}. On peut également utiliser des alias pour 
les motifs reconnus afin de les utiliser dans les actions (ici \texttt{lxm}). Les mots clés sont envoyés à l'analyseur syntaxique.

\subsection{Yacc}

\noindent \emph{YACC} (\textit{Yet Another Compiler Compiler}) g\'en\`ere des 
analyseurs syntaxiques. Un analyseur syntaxique va d\'eclancher des actions quand des structures grammaticales sont reconnues.

Les fichiers \emph{ocamlyacc} portent l'extension \texttt{.mly} et sont organisés en quatre parties~:

\begin{VerbProgram}
  %{
    <Entête>
  %}
    <Déclarations>
  %%
    <Règles>
  %%
    <Sous-programmes>
\end{VerbProgram}

\noindent Comme pour \emph{ocamllex}, les parties \texttt{<Entête>} et \texttt{<Sous-programmes>} contiennent du code \emph{Ocaml}.\\

\noindent La partie \texttt{<Déclarations>} contient la déclaration des 
mots reconnus par la grammaire et qui sont principalement envoyés par
l'analyseur lexical. Ces déclaration sont précédées de \texttt{\%token} 
et peuvent être écrites sur une seule ligne ou sur plusieurs~:

\begin{VerbProgram}
  %token <Liste de mots-clés>
  %token <Liste de mots-clés>
\end{VerbProgram}

\noindent Par exemple, en reprenant le dernier exemple donné à la 
section précédente sur \emph{Lex}, nous avons deux mots-clés (\texttt{NUM} et \texttt{TRUC}) à déclarer~:

\begin{VerbProgram}
  %token <float>NUM
  %token TRUC
\end{VerbProgram}

\noindent Le \texttt{<float>} collé avant \texttt{NUM} indique que 
\texttt{NUM} est de type \texttt{float}. Il peut s'agir de tout autre 
type \emph{Ocaml}.

Dans cette section, il faut également déclarer par quelle règle la 
grammaire commence à l'aide de \texttt{\%start} et quel est sont type. 
Par exemple, si notre première règle s'appelle \texttt{main} et que la 
structure renvoyée par l'analyseur est de type \texttt{Arbre}, on aura 
la déclaration suivante~:

\begin{VerbProgram}
  %start main
  %type <Arbre>main
\end{VerbProgram}

\noindent La partie \texttt{<Règles>} contient la grammaire qui est 
décrite sous une forme proche de BNF \footnote{Backus–Naur Form, 
grammaire décrite à l’aide de règles de dérivations de telle facon que 
les symboles n’apparaissant pas dans la partie gauche de la règle sont 
les symboles dits terminaux.}. Les règles syntaxiques sont écrites sous 
la forme suivante~ :

\begin{VerbProgram}
<Non-terminal> :
    <Symbole> … <Symbole> { <Action sémantique> }
  | …
  | <Symbole> … <Symbole> { <Action sémantique> }
;
\end{VerbProgram}

\noindent Comme son nom l'indique \emph{<Non-terminal>} représente un symbole non-
terminal de la grammaire qui constituera ici le nom des règles. \emph{Symbole}
désigne un symbole terminal ou non-terminal. Un symbole terminal sera ici un
caractère ou un mot-clé déclaré précédemment.

Les actions sémantiques sont des parties de codes \emph{Ocaml}. Elles utilisent les
variables \texttt{\$1}, \texttt{\$2}, etc. pour représenter les
valeurs prises par les symboles. \texttt{\$1} est par exemple la valeur du 
premier symbole.\\

\noindent La section \emph{exemples} de ce document présente des exemples complets.

\subsection{Ocaml}

\noindent Le TP devra être réalisé en \emph{Ocaml} avec \emph{ocamllex} et \emph{ocamlyacc}. Cependant, 
le but du TP n'étant d'aprendre le langage \emph{Ocaml} mais \emph{Lex} et \emph{Yacc}, les parties de code 
n\'ecessaires en \emph{Ocaml} seront donn\'ees. Sinon, n'h\'esitez pas \`a vous 
documenter par vous-m\^eme et poser des questions durant les TPs.

La documentation officielle d'\emph{Ocaml} est disponible sur internet \cite{dococaml}
ainsi que des ouvrage complets librement consultables \cite{livreocaml,realworldocaml}.

\section{Exemples}

\noindent Les exemples sont disponibles sur \emph{GitHub}~: \url{https://github.com/bguedas/Compilation-M1}.

\subsection{Exemple 1}

\noindent Le premier exemple se trouvant dans le répertoire \texttt{example1} ne contient
qu'un seul fichier \emph{ocamllex}. Vous pouvez le compiler avec ligne de commande
suivante~:

\begin{verbatim}
  shell# ocamllex example1_lexer.mll
\end{verbatim}

\noindent Vous verrez alors qu'un analyseur lexical en \emph{Ocaml} a été généré~: 
\texttt{example1\_lexer.ml}. Vous pouvez compiler cet analyseur comme un fichier 
\emph{Ocaml} classique~:

\begin{verbatim}
  shell# ocamlc example1_lexer.ml -o example1
\end{verbatim}

\noindent Vous avez maintenant un exécutable sur lequel vous pouvez essayer le fichier
de test \texttt{example1\_test.txt}~:

\begin{verbatim}
  shell# ./example1 example1_test.txt
\end{verbatim}

\subsection{Exemple 2}

\noindent Le second exemple est plus complet et contient un fichier \emph{ocamllex}, un fichier
\emph{ocamlyacc} et des fichiers de code \emph{Ocaml}. Il vous montre comment interfacer ces
différents fichiers et comment les compiler. Pour la compilation, un \emph{makefile}
est fourni. La commande suivante compilera l'exemple~:

\begin{verbatim}
  shell# make
\end{verbatim}

\noindent Vous pouvez étudier les fichiers créés. L'exécutable créé se nomme 
\texttt{example2} et vous pouvez l'essayer sur le fichier d'exemple 
\texttt{example2\_test.txt}~:

\begin{verbatim}
  shell# ./example2 example2_test.txt
\end{verbatim}

\noindent Pour effacer tous les fichiers créés à la compilation à l'exception de l'exécutable, utilisez la commande~:

\begin{verbatim}
  shell# make clean
\end{verbatim}

\subsection{Exemple 3}

\noindent Le troisième exemple se compile comme l'exemple~2~:

\begin{verbatim}
  shell# make
\end{verbatim}

\noindent L'exécution du programme affiche sur la sortie standard le contenu d'un fichier \emph{dot}.
La commande \texttt{dot} est un module du logiciel de tracé de graphes \emph{Graphviz}~\cite{graphviz,exdot}.\\

\noindent Vous pouvez rediriger la sortie dans un fichier~:

\begin{verbatim}
 shell# ./example3 example3_test.txt > example3_test.dot
\end{verbatim}

\noindent Le fichier \emph{dot} produit peut ensuite être transformé en image~:

\begin{verbatim}
 shell# dot exemple3_test.dot -Tpng -o example3_test.png
\end{verbatim}

\section{Plan des séances}

\begin{itemize}
  \item La première séance est une introduction générale à la compilation et une présentation des outils \emph{Lex} \& \emph{Yacc} ;
  \item La seconde séance est consacrée à l'analyse lexicale avec un programme à écrire en \emph{Lex} ;
  \item La troisième et la quatrième séance ajoutent l'analyse syntaxique et en particulier avec l'outil \emph{Yacc} ;
  \item Les six séances suivantes sont consacrées à un projet.
\end{itemize}


\begin{thebibliography}{1}
\bibitem[1]{dragon} \textit{Compilers -- Principles, Techniques, and Tools}, Alfred V. Aho,
Ravi Sethi, Jeffrey D. Ullman, Addison Wesley, 1988.
\bibitem[2]{flex} Site officiel de Flex : \url{http://flex.sourceforge.net/}
\bibitem[3]{bison} Site officiel de Bison : \url{http://www.gnu.org/software/bison/}
\bibitem[4]{doclexyacc} Lex et Yacc dans la doc officielle Ocaml~: \url{http://caml.inria.fr/pub/docs/manual-ocaml-4.00/manual026.html}
\bibitem[5]{exempleslexyacc} Exemples simples de Lex et Yacc avec Ocaml~: \url{http://www.matt-mcdonnell.com/code/code_ocaml/lexing/lexing.html}
\bibitem[6]{dococaml} Documentation officielle d'Ocaml~: \url{http://caml.inria.fr/pub/docs/manual-ocaml/index.html}
\bibitem[7]{livreocaml} Livre ancien, en anglais sur Ocaml : \url{http://caml.inria.fr/pub/docs/oreilly-book/index.html}
\bibitem[8]{realworldocaml} Livre récent, en anglais sur Ocaml : \url{https://realworldocaml.org/v1/en/html/index.html}
\bibitem[9]{graphviz} Site officiel de graphviz : \url{http://www.graphviz.org/}
\bibitem[10]{exdot} Gallerie d'exemples Graphviz : \url{http://www.graphviz.org/Gallery.php}
\end{thebibliography}
\end{feuille}

\end{document}

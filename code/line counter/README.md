Description
===========

A simple programme that counts the number of lines given from the standard 
input.

Compile
=======

First compile the ocamllex file to ocaml:

    ocamllex line_counter.mll

Then compile the ocaml file to an executable programme:

    ocamlc line_counter.ml -o line_counter

Run
===

You can run the program without arguments: 

    ./line_counter

and then just type whatever you want and **Ctrl+D** (end of file) when you're
done.

Or you can send a file to the program:

    ./line_count < line_count.mll

This prints the number of lines of *line_count.mll*.



{
  (**
   * The global variable 'count' is a reference to an integer so it can be
   * modified.
   *)
  let count = ref 0
}

(**
 * The one and only rule is here called 'main'.
 *)
rule main = parse
  (* When a new line character is read, increment the 'count' variable
   * and continue with the main rule. *)
  | '\n' {incr count; main lexbuf}
  (* When the end of input stream is reached, print the line count. *)
  | eof {Printf.printf "Number of lines: %d\n" !count}
  (* When a random character is read, just continue. *)
  | _   {main lexbuf}

{
  (**
   * The main function. It puts the standard input (stdin) in a lexing
   * buffer and calls the main rule on this buffer.
   *)
  let () =
    let lexbuf = Lexing.from_channel stdin in
    main lexbuf
}

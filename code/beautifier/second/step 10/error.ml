open Lexing

type beautifier_exception = string * Lexing.position

exception BeautifierError of beautifier_exception

let error message pos = raise (BeautifierError (message, pos))

let print (m,p) =
  Printf.eprintf "Error line %d character %d: %s\n" p.pos_lnum (p.pos_bol + 1) m
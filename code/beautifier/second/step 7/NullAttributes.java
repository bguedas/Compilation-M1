package com.guedas.samples;

import truc.machin;
import plop.re.de;

/**
 * An useless class definition
 */
public abstract class NullAttributes {

    private int count = 0;
    
    protected bool init = false;

    public final String name;
    
    public static void main(String[] args) {}
}
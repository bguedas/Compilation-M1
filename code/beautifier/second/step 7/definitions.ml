let red = "\x1b[31m"
let green = "\x1b[32m"
let yellow = "\x1b[33m"
let blue = "\x1b[34m"
let magenta = "\x1b[35m"
let cyan = "\x1b[36m"

let light_red = "\x1b[91m"
let light_green = "\x1b[92m"
let light_yellow = "\x1b[93m"
let light_blue = "\x1b[94m"
let light_magenta = "\x1b[95m"
let light_cyan = "\x1b[96m"

let reset () = print_string "\x1b[39;49m"

(** Prints a string with the given color. *)
let print_string_c clr str =
  print_string clr;
  print_string str;
  reset ()
  
type t_expression =
  | NoExpr
  | Bool of string
  | Num of string
  
type t_variable = {
  var_modifiers: string list;
  var_type: string;
  var_name: string ;
  var_value: t_expression
}

type t_attr_or_meth =
  | Attribute of t_variable
  | Method of t_variable * t_variable list

(*
 * The java object type is either a class, an interface or an enum and has
 * name.
 *)
type t_object = {
  class_modifiers: string list;
  class_type: string;
  class_name: string;
  class_decl: t_attr_or_meth list
}

type t_file = {
  package: string;
  imports: string list;
  object_def: t_object
}

(* The indentation is 4 spaces. *)
let tab = "    "

let rec print_modifiers = function
 | [] -> ()
 | hd :: tl -> print_string_c blue hd ; print_char ' ' ; print_modifiers tl

let print_package = function
 | "" -> ()
 | p -> print_string_c light_red "package ";
        print_string p;
        print_char ';';
        print_newline ();
        print_newline ()
         
let rec print_imports = function
 | [] -> ()
 | hd :: tl -> print_string_c light_red "import ";
               print_string hd;
               print_endline ";";
               print_imports tl;
               print_newline ()
               
let print_type t =
  print_string_c yellow t;
  print_char ' '

let print_expr = function
  | NoExpr -> ()
  | Bool b -> print_string_c cyan b
  | Num n -> print_string_c magenta n
  
let print_variable var indent =
  print_string indent;
  print_modifiers var.var_modifiers;
  print_type var.var_type;
  print_string var.var_name;
  if var.var_value != NoExpr
    then print_string " = ";
         print_expr var.var_value
  
let print_attribute attr ident =
  print_variable attr ident;
  print_endline ";"

let rec print_args = function
  | [] -> ()
  | [arg] -> print_variable arg ""
  | hd :: tl -> print_variable hd "";
                print_string ", ";
                print_args tl
  
let print_method meth args indent =
  print_variable meth indent;
  print_char '(';
  print_args args;
  print_string ") ";
  print_endline "{}"
  
let rec print_declarations decl indent = match decl with
 | [] -> ()
 | (Attribute attr) :: tl
    -> print_attribute attr indent;
       print_declarations tl indent
 | (Method (meth, args)) :: tl
    -> print_method meth args indent;
       print_declarations tl indent
 
(** Prints the beautified java file. *)
let print_java_object java =
  print_modifiers java.class_modifiers;
  print_string_c light_red java.class_type;
  print_char ' ';
  print_string java.class_name;
  print_endline " {";
  print_declarations java.class_decl tab;
  print_endline "}"
  
let print_java java =
  print_package java.package;
  print_imports java.imports;
  print_java_object java.object_def
let red = "\x1b[31m"
let green = "\x1b[32m"
let yellow = "\x1b[33m"
let blue = "\x1b[34m"
let magenta = "\x1b[35m"
let cyan = "\x1b[36m"

let light_red = "\x1b[91m"
let light_green = "\x1b[92m"
let light_yellow = "\x1b[93m"
let light_blue = "\x1b[94m"
let light_magenta = "\x1b[95m"
let light_cyan = "\x1b[96m"

let reset () = print_string "\x1b[39;49m"

(** Prints a string with the given color. *)
let print_string_c clr str =
  print_string clr;
  print_string str;
  reset ()

(*
 * The java object type is either a class, an interface or an enum and has
 * name.
 *)
type java_object = {
  class_modifiers: string list;
  class_type: string;
  class_name: string
}

let rec print_modifiers = function
 | [] -> ()
 | hd :: tl -> print_string_c blue hd ; print_char ' ' ; print_modifiers tl

(** Prints the beautified java file. *)
let print_java java =
  print_modifiers java.class_modifiers;
  print_string_c light_red java.class_type;
  print_char ' ';
  print_string java.class_name;
  print_endline " {}"
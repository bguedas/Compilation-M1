%{
  open Definitions
  
  let build_class m t n =
    {class_modifiers = m ; class_type = t ; class_name = n}
%}

%token LBRACE RBRACE EOF
%token<string> MODIFIER CLASS IDENTIFIER
%start java_file
%type <Definitions.java_object> java_file
%%

java_file:
   class_def EOF {$1}
;

class_def:
   modifiers CLASS IDENTIFIER LBRACE RBRACE {build_class $1 (List.rev $2) $3}
;

modifiers:
   /* empty */ {[]}
 | modifiers MODIFIER {$2 :: $1}
;
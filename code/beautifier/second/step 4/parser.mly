%{
  open Definitions
  
  let buid_attribute m t n =
    {attr_modifiers = m ; attr_type = t ; attr_name = n}
  
  let build_class m t n d =
    {class_modifiers = m ; class_type = t ; class_name = n; class_decl = d}
    
  let build_java_file p i o =
    {package = p ; imports = i ; object_def = o}
%}

%token PACKAGE IMPORT LBRACE RBRACE SEMICOLON DOT EOF
%token<string> MODIFIER CLASS TYPE IDENTIFIER
%start java_file
%type <Definitions.java_file> java_file
%%

java_file:
   package_def imports class_def EOF {build_java_file $1 (List.rev $2) $3}
;

package_def:
   /* empty */ {""}
 | PACKAGE qualified_identifier SEMICOLON {$2}
;

imports:
   /* empty */ {[]}
 | imports import {$2 :: $1}
;

import:
   IMPORT qualified_identifier SEMICOLON {$2}
;

class_def:
   modifiers CLASS IDENTIFIER LBRACE attributes_and_methods RBRACE 
     {build_class (List.rev $1) $2 $3 (List.rev $5)}
;

attributes_and_methods:
    /* empty */ {[]}
 | attributes_and_methods attribute_or_method {$2 :: $1}
;

attribute_or_method:
    modifiers java_type qualified_identifier SEMICOLON
      {buid_attribute (List.rev $1) $2 $3}
;

modifiers:
   /* empty */ {[]}
 | modifiers MODIFIER {$2 :: $1}
;

java_type:
   TYPE {$1}
 | qualified_identifier {$1}
;

qualified_identifier:
   IDENTIFIER {$1}
 | qualified_identifier DOT IDENTIFIER {$1 ^ "." ^ $3}
;
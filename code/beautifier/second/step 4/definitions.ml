let red = "\x1b[31m"
let green = "\x1b[32m"
let yellow = "\x1b[33m"
let blue = "\x1b[34m"
let magenta = "\x1b[35m"
let cyan = "\x1b[36m"

let light_red = "\x1b[91m"
let light_green = "\x1b[92m"
let light_yellow = "\x1b[93m"
let light_blue = "\x1b[94m"
let light_magenta = "\x1b[95m"
let light_cyan = "\x1b[96m"

let reset () = print_string "\x1b[39;49m"

(** Prints a string with the given color. *)
let print_string_c clr str =
  print_string clr;
  print_string str;
  reset ()
  
type attribute = {
  attr_modifiers: string list;
  attr_type: string;
  attr_name: string
}

(*
 * The java object type is either a class, an interface or an enum and has
 * name.
 *)
type java_object = {
  class_modifiers: string list;
  class_type: string;
  class_name: string;
  class_decl: attribute list
}

type java_file = {
  package: string;
  imports: string list;
  object_def: java_object
}

(* The indentation is 4 spaces. *)
let tab = "    "

let rec print_modifiers = function
 | [] -> ()
 | hd :: tl -> print_string_c blue hd ; print_char ' ' ; print_modifiers tl

let print_package = function
 | "" -> ()
 | p -> print_string_c light_red "package ";
        print_string p;
        print_char ';';
        print_newline ();
        print_newline ()
         
let rec print_imports = function
 | [] -> ()
 | hd :: tl -> print_string_c light_red "import ";
               print_string hd;
               print_endline ";";
               print_imports tl;
               print_newline ()
               
let print_type t =
  print_string_c yellow t;
  print_char ' '
               
let print_attribute attr ident =
  print_string ident;
  print_modifiers attr.attr_modifiers;
  print_type attr.attr_type;
  print_string attr.attr_name;
  print_endline ";"
  
let rec print_attributes attrs ident = match attrs with
 | [] -> ()
 | hd :: tl -> print_attribute hd ident;
               print_attributes tl ident
 
(** Prints the beautified java file. *)
let print_java_object java =
  print_modifiers java.class_modifiers;
  print_string_c light_red java.class_type;
  print_char ' ';
  print_string java.class_name;
  print_endline " {";
  print_attributes java.class_decl tab;
  print_endline "}"
  
let print_java java =
  print_package java.package;
  print_imports java.imports;
  print_java_object java.object_def
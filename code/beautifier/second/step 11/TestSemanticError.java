package com.guedas.samples;

import truc.machin;
import plop.re.de;

/**
 * The attribute init is declared twice.
 */
public abstract class TestSemanticError {

    private int count = 0;
    
    protected bool init = false;

    public final String name;
    
    public TestSemanticError() {}
    
    private String init = null;
    
    public static void main(String[] args) {
        final int i = 0;
        System.out.println(args[0]);
        nothing(count);
    }
    
    private void nothing(int i) {
    }
}
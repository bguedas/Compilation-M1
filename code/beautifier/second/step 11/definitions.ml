let red = "\x1b[31m"
let green = "\x1b[32m"
let yellow = "\x1b[33m"
let blue = "\x1b[34m"
let magenta = "\x1b[35m"
let cyan = "\x1b[36m"

let light_red = "\x1b[91m"
let light_green = "\x1b[92m"
let light_yellow = "\x1b[93m"
let light_blue = "\x1b[94m"
let light_magenta = "\x1b[95m"
let light_cyan = "\x1b[96m"

let reset () = print_string "\x1b[39;49m"

(** Prints a string with the given color. *)
let print_string_c clr str =
  print_string clr;
  print_string str;
  reset ()
  
module DefAttr = Set.Make(String)
  
type t_expression =
  | NoExpr
  | Bool of string
  | Num of string
  | Var of string
  | Array of string * t_expression list

type t_fun_call = {
  fun_name: string;
  fun_params: t_expression list
}  
  
type t_type =
  | Constructor
  | JavaType of string
  
type t_variable = {
  var_modifiers: string list;
  var_type: t_type;
  var_name: string ;
  var_value: t_expression
}

type t_statement =
 | VarDecl of t_variable
 | FunCall of t_fun_call

type t_method = {
  meth_decl: t_variable;
  meth_args: t_variable list;
  meth_statements: t_statement list
}

type t_attr_or_meth =
  | Attribute of t_variable
  | Method of t_method

(*
 * The java object type is either a class, an interface or an enum and has
 * name.
 *)
type t_object = {
  class_modifiers: string list;
  class_type: string;
  class_name: string;
  class_decl: t_attr_or_meth list
}

type t_file = {
  package: string;
  imports: string list;
  object_def: t_object
}

(* The indentation is 4 spaces. *)
let tab = "    "

let rec print_modifiers = function
 | [] -> ()
 | hd :: tl -> print_string_c blue hd ; print_char ' ' ; print_modifiers tl

let print_package = function
 | "" -> ()
 | p -> print_string_c light_red "package ";
        print_string p;
        print_char ';';
        print_newline ();
        print_newline ()
         
let rec print_imports = function
 | [] -> ()
 | hd :: tl -> print_string_c light_red "import ";
               print_string hd;
               print_endline ";";
               print_imports tl;
               print_newline ()
               
let print_type = function
 | Constructor -> ()
 | JavaType t -> print_string_c yellow t;
                 print_char ' '

let print_seen var seen =
  if DefAttr.mem var seen
    then print_string_c light_cyan var
    else print_string var
                 
let rec print_expr expr seen = match expr with
 | NoExpr -> ()
 | Bool b -> print_string_c cyan b
 | Num n -> print_string_c magenta n
 | Var v -> print_seen v seen
 | Array (v, is) -> print_string v;
                    print_indicies is seen
                
and print_indicies ids seen = match ids with
 | [] -> ()
 | hd :: tl -> print_char '[';
               print_expr hd seen;
               print_char ']';
               print_indicies tl seen
               
let print_variable var indent seen =
  print_string indent;
  print_modifiers var.var_modifiers;
  print_type var.var_type;
  print_seen var.var_name seen;
  if var.var_value != NoExpr then
    print_string " = ";
    print_expr var.var_value seen
  
let print_attribute attr indent seen =
  print_variable attr indent seen ;
  print_endline ";"

let rec print_args var seen = match var with
  | [] -> ()
  | [arg] -> print_variable arg "" seen
  | hd :: tl -> print_variable hd "" seen;
                print_string ", ";
                print_args tl seen

let rec print_params params seen = match params with
 | [] -> ()
 | [p] -> print_expr p seen
 | hd :: tl -> print_expr hd seen;
               print_string ", ";
               print_params tl seen
                
let print_statement stmts indent seen = match stmts with
 | VarDecl v -> print_variable v indent seen
 | FunCall f -> print_string indent;
                print_string f.fun_name;
                print_string " (";
                print_params f.fun_params seen;
                print_char ')'

let rec print_statements stmts indent seen = match stmts with
 | [] -> ()
 | hd :: tl -> print_statement hd indent seen;
               print_endline ";";
               print_statements tl indent seen
                
let print_method meth indent seen =
  print_variable meth.meth_decl indent seen;
  print_char '(';
  print_args meth.meth_args seen;
  print_endline ") {";
  print_statements meth.meth_statements (indent ^ tab) seen;
  print_string indent;
  print_endline "}"
  
let rec print_declarations decl indent seen = match decl with
 | [] -> ()
 | (Attribute attr) :: tl
    -> print_attribute attr indent seen;
       print_declarations tl indent seen
 | (Method meth) :: tl
    -> print_method meth indent seen;
       print_declarations tl indent seen
 
(** Prints the beautified java file. *)
let print_java_object java seen =
  print_modifiers java.class_modifiers;
  print_string_c light_red java.class_type;
  print_char ' ';
  print_string java.class_name;
  print_endline " {";
  print_declarations java.class_decl tab seen;
  print_endline "}"
  
let print_java java seen =
  print_package java.package;
  print_imports java.imports;
  print_java_object java.object_def seen
{
  (* Open the parser module for the tokens declarations. *)
  open Parser
  (* Open the lexing module to update lexbuf position. *)
  open Lexing
  
  (** Increments the lexing buffer line number counter.*)
  let incr_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      {pos with pos_lnum = pos.pos_lnum + 1; pos_bol = 0}
  
  (** Increments the lexing buffer line offset by the given length. *)
  let incr_bol lexbuf length =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <- {pos with pos_bol = pos.pos_bol + length}
    
  (** Increments the lexing buffer line offset by the given lexem length. *)
  let incr_bol_lxm lexbuf lxm = incr_bol lexbuf (String.length lxm)
  
  (** Turns a char into a string containing this char. *)
  let string_of_char c = String.make 1 c
}

let alpha_char = ['a'-'z' 'A'-'Z' '_']
let digit = ['0'-'9']
let identifier = alpha_char (alpha_char | digit)*
let integer = digit+
(** The main lexing rule. *)
rule token = parse
  (* Tokens to send to the parser. *)
  | "package" {incr_bol lexbuf 7 ; PACKAGE}
  | "import" {incr_bol lexbuf 6 ; IMPORT}
  | "public" | "private" | "protected" | "static" | "abstract"
  | "final" as lxm {incr_bol_lxm lexbuf lxm ; MODIFIER lxm} 
  | "class" | "interface" | "enum" as lxm {incr_bol_lxm lexbuf lxm ; CLASS lxm}
  | "int" | "float" | "long" | "double"
  | "byte" | "bool" | "void" as lxm {incr_bol_lxm lexbuf lxm ; TYPE lxm}
  | "true" | "false" as lxm {incr_bol_lxm lexbuf lxm ; BOOL lxm}
  | identifier as lxm {incr_bol_lxm lexbuf lxm ; IDENTIFIER lxm}
  | integer as lxm {incr_bol_lxm lexbuf lxm ; INT lxm}
  | '{' {incr_bol lexbuf 1 ; LBRACE}
  | '}' {incr_bol lexbuf 1 ; RBRACE}
  | '(' {incr_bol lexbuf 1 ; LPAR}
  | ')' {incr_bol lexbuf 1 ; RPAR}
  | '[' {incr_bol lexbuf 1 ; LBRACK}
  | ']' {incr_bol lexbuf 1 ; RBRACK}
  | ';' {incr_bol lexbuf 1 ; SEMICOLON}
  | '.' {incr_bol lexbuf 1 ; DOT}
  | ',' {incr_bol lexbuf 1 ; COMMA}
  | '=' {incr_bol lexbuf 1 ; ASSIGN}
  | eof {EOF}
  (* New line *)
  | '\n' {incr_line lexbuf ; token lexbuf}
  (* Skip cariage return *)
  | '\r' {token lexbuf}
  (* White space *)
  | ' ' | '\t' {incr_bol lexbuf 1 ; token lexbuf}
  (* Comments *)
  | "//" {l_comment lexbuf}
  | "/*" {incr_bol lexbuf 2 ; m_comment lexbuf}
  (* Raise an exception with all unknown characters *)
  | _ as c {Error.error ("Unrecognized character " ^ (string_of_char c)) lexbuf.lex_curr_p}

(** Skip line comments rule. *)
and l_comment = parse
  (* Return to the main rule at the end of line *)
  | '\n' {incr_line lexbuf ; token lexbuf}
  (* End of file *)
  | eof {EOF}
  (* Continue whatever *)
  | _ {l_comment lexbuf}
  
(** Skip multiline comments rule. *)
and m_comment = parse
  (* Return to the main rule at the end of the comment *)
  | "*/" {incr_bol lexbuf 2 ; token lexbuf}
  (* Fails on end of file *)
  | eof {Error.error "Unexpected end of file in a comment" lexbuf.lex_curr_p}
  (* New line *)
  | '\n' {incr_line lexbuf ; m_comment lexbuf}
  (* Continue whatever comes *)
  | _ {incr_bol lexbuf 1 ; m_comment lexbuf}
  
{}
%{
  open Definitions
  
  let build_variable m t n v =
    {var_modifiers = m ; var_type = t ; var_name = n ; var_value = v}
  
  let  build_fun_call n p = {fun_name = n ; fun_params = p}
  
  let build_attribute m t n v = Attribute (build_variable m t n v)
    
  let build_method m t n a s =
    Method {
      meth_decl = build_variable m t n NoExpr; 
      meth_args = a;
      meth_statements  = s
    }
  
  let build_class m t n d =
    {class_modifiers = m ; class_type = t ; class_name = n; class_decl = d}
    
  let build_java_file p i o =
    {package = p ; imports = i ; object_def = o}
    
  (** Overrides the default parse_error function. *)  
  let parse_error s = Error.error "Parsing error" (symbol_start_pos ())
  
  let def_attr = ref DefAttr.empty
  
  let add_attr var =
    let attr = var.var_name in
    if DefAttr.mem attr !def_attr
      then Error.warning (attr ^ " has already been declared") (symbol_start_pos ())
      else def_attr := DefAttr.add attr !def_attr
%}

%token PACKAGE IMPORT FINAL LBRACE RBRACE LBRACK RBRACK LPAR RPAR
%token COMMA SEMICOLON DOT ASSIGN EOF
%token<string> MODIFIER CLASS TYPE BOOL IDENTIFIER INT
%start java_file
%type <Definitions.t_file * Definitions.DefAttr.t> java_file
%%

java_file:
   package_def imports class_def EOF {(build_java_file $1 (List.rev $2) $3), !def_attr}
;

package_def:
   /* empty */ {""}
 | PACKAGE qualified_identifier SEMICOLON {$2}
;

imports:
   /* empty */ {[]}
 | imports import {$2 :: $1}
;

import:
   IMPORT qualified_identifier SEMICOLON {$2}
;

class_def:
   modifiers CLASS IDENTIFIER LBRACE attributes_and_methods RBRACE 
     {build_class (List.rev $1) $2 $3 (List.rev $5)}
;

attributes_and_methods:
    /* empty */ {[]}
 | attributes_and_methods attribute_or_method {$2 :: $1}
;

attribute_or_method:
   attribute_def {$1}
 | method_def {$1}
;

attribute_def:
   variable_decl SEMICOLON {add_attr $1 ; Attribute $1}
;

expr:
   BOOL {Bool $1}
 | INT {Num $1}
 | qualified_identifier {Var $1}
 | qualified_identifier array_indicies {Array ($1, $2)}
;

array_indicies:
   LBRACK expr RBRACK {[$2]}
 | array_indicies LBRACK expr RBRACK {$1 @ [$3]}
;

method_def:
   modifiers java_type qualified_identifier LPAR args RPAR LBRACE statements RBRACE
     {build_method (List.rev $1) $2 $3 $5 (List.rev $8)}
 | modifiers qualified_identifier LPAR args RPAR LBRACE statements RBRACE
     {build_method (List.rev $1) Constructor $2 $4 $7}
;

statements:
   /* empty */ {[]}
 | statements statement {$2 :: $1}
;

statement:
   variable_decl SEMICOLON {VarDecl $1}
 | function_call SEMICOLON {FunCall $1}
;

variable_decl:
   modifiers java_type IDENTIFIER
     {build_variable (List.rev $1) $2 $3 NoExpr}
 | modifiers java_type IDENTIFIER ASSIGN expr
     {build_variable (List.rev $1) $2 $3 $5}
;

function_call:
  qualified_identifier LPAR params RPAR {build_fun_call $1 $3}
;

modifiers:
   /* empty */ {[]}
 | modifiers MODIFIER {$2 :: $1}
;

args:
   /* empty */ {[]}
 | arg_list {List.rev $1}
;

arg_list:
   arg {[$1]}
 | arg_list COMMA arg {$3 :: $1}
;

arg:
   modifiers java_type IDENTIFIER {build_variable $1 $2 $3 NoExpr}
;

params:
   /* empty */ {[]}
 | param_list {$1}
;

param_list:
   expr {[$1]}
 | param_list COMMA expr {$3 :: $1}
;

java_type:
  java_type_string {JavaType $1}
;

java_type_string:
   TYPE {$1}
 | qualified_identifier {$1}
 | java_type_string LBRACK RBRACK {$1 ^ "[]"}
;

qualified_identifier:
   IDENTIFIER {$1}
 | qualified_identifier DOT IDENTIFIER {$1 ^ "." ^ $3}
;
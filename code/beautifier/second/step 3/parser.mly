%{
  open Definitions
  
  let build_class m t n =
    {class_modifiers = m ; class_type = t ; class_name = n}
    
  let build_java_file p i o =
    {package = p ; imports = i ; object_def = o}
%}

%token PACKAGE IMPORT LBRACE RBRACE SEMICOLON DOT EOF
%token<string> MODIFIER CLASS IDENTIFIER
%start java_file
%type <Definitions.java_file> java_file
%%

java_file:
   package_def imports class_def EOF {build_java_file $1 (List.rev $2) $3}
;

package_def:
   /* empty */ {""}
 | PACKAGE qualified_identifier SEMICOLON {$2}
;

imports:
   /* empty */ {[]}
 | imports import {$2 :: $1}
;

import:
   IMPORT qualified_identifier SEMICOLON {$2}
;

class_def:
   modifiers CLASS IDENTIFIER LBRACE RBRACE {build_class (List.rev $1) $2 $3}
;

modifiers:
   /* empty */ {[]}
 | modifiers MODIFIER {$2 :: $1}
;

qualified_identifier:
   IDENTIFIER {$1}
 | qualified_identifier DOT IDENTIFIER {$1 ^ "." ^ $3}
;
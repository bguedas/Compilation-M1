%{
  open Definitions
  
  let build_variable m t n =
    {var_modifiers = m ; var_type = t ; var_name = n}
  
  let build_attribute m t n = Attribute (build_variable m t n)
    
  let build_method m t n a = Method ((build_variable m t n), a)
  
  let build_class m t n d =
    {class_modifiers = m ; class_type = t ; class_name = n; class_decl = d}
    
  let build_java_file p i o =
    {package = p ; imports = i ; object_def = o}
%}

%token PACKAGE IMPORT FINAL LBRACE RBRACE LBRACK RBRACK LPAR RPAR
%token COMMA SEMICOLON DOT EOF
%token<string> MODIFIER CLASS TYPE IDENTIFIER
%start java_file
%type <Definitions.t_file> java_file
%%

java_file:
   package_def imports class_def EOF {build_java_file $1 (List.rev $2) $3}
;

package_def:
   /* empty */ {""}
 | PACKAGE qualified_identifier SEMICOLON {$2}
;

imports:
   /* empty */ {[]}
 | imports import {$2 :: $1}
;

import:
   IMPORT qualified_identifier SEMICOLON {$2}
;

class_def:
   modifiers CLASS IDENTIFIER LBRACE attributes_and_methods RBRACE 
     {build_class (List.rev $1) $2 $3 (List.rev $5)}
;

attributes_and_methods:
    /* empty */ {[]}
 | attributes_and_methods attribute_or_method {$2 :: $1}
;

attribute_or_method:
   attribute_def {$1}
 | method_def {$1}
;

attribute_def:
   modifiers java_type qualified_identifier SEMICOLON
     {build_attribute (List.rev $1) $2 $3}
;

method_def:
   modifiers java_type qualified_identifier LPAR args RPAR LBRACE RBRACE
     {build_method (List.rev $1) $2 $3 $5}
;

modifiers:
   /* empty */ {[]}
 | modifiers MODIFIER {$2 :: $1}
;

args:
   /* empty */ {[]}
 | arg_list {List.rev $1}
;

arg_list:
   arg {[$1]}
 | arg_list COMMA arg {$3 :: $1}
;

arg:
   modifiers java_type IDENTIFIER {build_variable $1 $2 $3}
;

java_type:
   TYPE {$1}
 | qualified_identifier {$1}
 | java_type LBRACK RBRACK {$1 ^ "[]"}
;

qualified_identifier:
   IDENTIFIER {$1}
 | qualified_identifier DOT IDENTIFIER {$1 ^ "." ^ $3}
;
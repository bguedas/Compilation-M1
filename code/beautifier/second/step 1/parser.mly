%{
  open Definitions
%}

%token CLASS LBRACE RBRACE EOF
%token<string> IDENTIFIER
%start java_file
%type <Definitions.java_class> java_file
%%

java_file:
  class_def EOF {$1}
;

class_def:
  CLASS IDENTIFIER LBRACE RBRACE {$2}
;

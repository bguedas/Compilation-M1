{
  (* Open the parser module for the tokens declarations. *)
  open Parser
}

let alpha_char = ['a'-'z' 'A'-'Z' '_']
let digit = ['0'-'9']
let identifier = alpha_char (alpha_char | digit)*

(** The main lexing rule. *)
rule token = parse
  (* Tokens to send to the parser. *)
  | "class" {CLASS}
  | identifier as lxm {IDENTIFIER lxm}
  | '{' {LBRACE}
  | '}' {RBRACE}
  | eof {EOF}

  (* Skip white spaces *)
  | ' ' | '\t' | '\r' | '\n' {token lexbuf}
  (* Comments *)
  | "//" {l_comment lexbuf}
  | "/*" {m_comment lexbuf}
  (* Raise an exception with all unknown characters *)
  | _ as c {failwith ("Unrecognized character: " ^ (String.make 1 c))}

(** Skip line comments rule. *)
and l_comment = parse
  (* Return to the main rule at the end of line *)
  | '\n' {token lexbuf}
  (* End of file *)
  | eof {EOF}
  (* Continue whatever *)
  | _ {l_comment lexbuf}
  
(** Skip multiline comments rule. *)
and m_comment = parse
  (* Return to the main rule at the end of the comment *)
  | "*/" {token lexbuf}
  (* Fails on end of file *)
  | eof {failwith "Comment not closed"}
  (* Continue whatever *)
  | _ {m_comment lexbuf}
  
{}
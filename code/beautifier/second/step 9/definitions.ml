let red = "\x1b[31m"
let green = "\x1b[32m"
let yellow = "\x1b[33m"
let blue = "\x1b[34m"
let magenta = "\x1b[35m"
let cyan = "\x1b[36m"

let light_red = "\x1b[91m"
let light_green = "\x1b[92m"
let light_yellow = "\x1b[93m"
let light_blue = "\x1b[94m"
let light_magenta = "\x1b[95m"
let light_cyan = "\x1b[96m"

let reset () = print_string "\x1b[39;49m"

(** Prints a string with the given color. *)
let print_string_c clr str =
  print_string clr;
  print_string str;
  reset ()
  
type t_expression =
  | NoExpr
  | Bool of string
  | Num of string
  | Var of string
  | Array of string * t_expression list

type t_fun_call = {
  fun_name: string;
  fun_params: t_expression list
}  
  
type t_type =
  | Constructor
  | JavaType of string
  
type t_variable = {
  var_modifiers: string list;
  var_type: t_type;
  var_name: string ;
  var_value: t_expression
}

type t_statement =
 | VarDecl of t_variable
 | FunCall of t_fun_call

type t_method = {
  meth_decl: t_variable;
  meth_args: t_variable list;
  meth_statements: t_statement list
}

type t_attr_or_meth =
  | Attribute of t_variable
  | Method of t_method

(*
 * The java object type is either a class, an interface or an enum and has
 * name.
 *)
type t_object = {
  class_modifiers: string list;
  class_type: string;
  class_name: string;
  class_decl: t_attr_or_meth list
}

type t_file = {
  package: string;
  imports: string list;
  object_def: t_object
}

(* The indentation is 4 spaces. *)
let tab = "    "

let rec print_modifiers = function
 | [] -> ()
 | hd :: tl -> print_string_c blue hd ; print_char ' ' ; print_modifiers tl

let print_package = function
 | "" -> ()
 | p -> print_string_c light_red "package ";
        print_string p;
        print_char ';';
        print_newline ();
        print_newline ()
         
let rec print_imports = function
 | [] -> ()
 | hd :: tl -> print_string_c light_red "import ";
               print_string hd;
               print_endline ";";
               print_imports tl;
               print_newline ()
               
let print_type = function
 | Constructor -> ()
 | JavaType t -> print_string_c yellow t;
                 print_char ' '

                 
let rec print_expr = function
 | NoExpr -> ()
 | Bool b -> print_string_c cyan b
 | Num n -> print_string_c magenta n
 | Var v -> print_string v
 | Array (v, is) -> print_string v;
                    print_indicies is
                
and print_indicies = function
 | [] -> ()
 | hd :: tl -> print_char '[';
               print_expr hd;
               print_char ']';
               print_indicies tl
 
let print_variable var indent =
  print_string indent;
  print_modifiers var.var_modifiers;
  print_type var.var_type;
  print_string var.var_name;
  if var.var_value != NoExpr then
    print_string " = ";
    print_expr var.var_value
  
let print_attribute attr ident =
  print_variable attr ident;
  print_endline ";"

let rec print_args = function
  | [] -> ()
  | [arg] -> print_variable arg ""
  | hd :: tl -> print_variable hd "";
                print_string ", ";
                print_args tl

let rec print_params = function
 | [] -> ()
 | [p] -> print_expr p
 | hd :: tl -> print_expr hd;
               print_string ", ";
               print_params tl
                
let print_statement stmts indent = match stmts with
 | VarDecl v -> print_variable v indent
 | FunCall f -> print_string indent;
                print_string f.fun_name;
                print_string " (";
                print_params f.fun_params;
                print_char ')'

let rec print_statements stmts indent = match stmts with
 | [] -> ()
 | hd :: tl -> print_statement hd indent;
               print_endline ";";
               print_statements tl indent
                
let print_method meth indent =
  print_variable meth.meth_decl indent;
  print_char '(';
  print_args meth.meth_args;
  print_endline ") {";
  print_statements meth.meth_statements (indent ^ tab);
  print_string indent;
  print_endline "}"
  
let rec print_declarations decl indent = match decl with
 | [] -> ()
 | (Attribute attr) :: tl
    -> print_attribute attr indent;
       print_declarations tl indent
 | (Method meth) :: tl
    -> print_method meth indent;
       print_declarations tl indent
 
(** Prints the beautified java file. *)
let print_java_object java =
  print_modifiers java.class_modifiers;
  print_string_c light_red java.class_type;
  print_char ' ';
  print_string java.class_name;
  print_endline " {";
  print_declarations java.class_decl tab;
  print_endline "}"
  
let print_java java =
  print_package java.package;
  print_imports java.imports;
  print_java_object java.object_def
%{
  open Definitions
%}

%token EOF
%token LBRACE RBRACE SEMICOLON
%token <string * string> KEYWORD

%start main
%type <Definitions.t_program list> main
%%

main:
  programs EOF {List.rev $1}
;

programs:
   /* empty */ {[]}
 | programs KEYWORD {(Keyword $2) :: $1}
 | programs SEMICOLON {Semicolon :: $1}
 | programs LBRACE programs RBRACE {(Block (List.rev $3)) :: $1}
;

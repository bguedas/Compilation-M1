let red = "\x1b[31m"
let green = "\x1b[32m"
let yellow = "\x1b[33m"
let blue = "\x1b[34m"
let magenta = "\x1b[35m"
let cyan = "\x1b[36m"

let light_red = "\x1b[91m"
let light_green = "\x1b[92m"
let light_yellow = "\x1b[93m"
let light_blue = "\x1b[94m"
let light_magenta = "\x1b[95m"
let light_cyan = "\x1b[96m"

let default = ""

let reset () = print_string "\x1b[39;49m"

(** Prints a string with the given color. *)
let print_string_c clr str =
  print_string clr;
  print_string str;
  reset ()

(** Prints a character with the given color. *)
let print_char_c clr chr =
  print_string clr;
  print_char chr;
  reset ()

type t_program =
 | Keyword of (string * string)
 | Semicolon
 | Block of t_program list

let rec print_program_aux pgm tab first = match pgm with
 | [] -> ()
 | hd::tl -> match hd with
     | Keyword (clr,kwd)
       -> if first then print_string tab else ();
          print_string_c clr kwd;
          print_char ' ';
          print_program_aux tl tab false
     | Semicolon
       -> print_endline ";" ;
          print_program_aux tl tab true
     | Block block
       -> print_endline "{";
          print_program_aux block ("  " ^ tab) true;
          print_endline (tab ^ "}");
          print_program_aux tl tab true

let print_program pgm = print_program_aux pgm "" true

{
  open Parser
  open Definitions

  let string_of_char c = String.make 1 c
}

let ws = [' ' '\t' '\r' '\n']

let integer  = ['0'-'9']+
let long = integer ('l' | 'L')
let float_or_double   = integer '.' integer ('D' | 'd' | 'F' | 'f')?
let number = integer | long | float_or_double
 
let p_type = "void" | "byte" | "int" | "short" | "long" | "float"
  | "double" | "bool" | "char"
let identifier = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*

let integer  = ['0'-'9']+
let long = integer ('l' | 'L')
let float_or_double   = integer '.' integer ('D' | 'd' | 'F' | 'f')?
let number = integer | long | float_or_double
let p_type = "void" | "byte" | "int" | "short" | "long" | "float"
  | "double" | "bool" | "char"
let ident = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let qualified_ident = ident ('.' ident)*
let pa_type = p_type? ('[' ']')? 

rule main = parse
  | "package"
  | "import"
  | "class"
  | "interface"
  | "enum"
  | "extends"
  | "implements"
  | "public"
  | "private"
  | "protected"
  | "static"
  | "abstract"
  | "final"
  | "new"
  | "thows"
  | "try"
  | "catch"
  | "finally"
  | "for"
  | "while"
  | "do"
  | "if"
  | "else"
  | "switch"
  | "case"
  | "return"
  | "instanceof" as lxm {KEYWORD (blue, lxm)}
  | "null" {KEYWORD (light_red, "null")}

  (* character operators *)
  | '|' | '!'
  | '+' | '-' | '*' | '/'
  | '=' | '<' | '>' as lxm {KEYWORD (yellow, string_of_char lxm)}

  (* double character operators *)
  | "||" | "&&"
  | "==" | "++" | "--" as lxm {KEYWORD (yellow, lxm)}

  (* characters *)
  | ';' {SEMICOLON}
  | '{' {LBRACE}
  | '}' {RBRACE}
  | ':'
  | '(' | ')'  as lxm {KEYWORD (default, string_of_char lxm)}

   (* primitive types including arrays *)
  | pa_type as lxm {KEYWORD (light_red, lxm)}
 
  (* identifiers *)
  | qualified_ident as lxm {KEYWORD (default, lxm)}

  | number as lxm {KEYWORD (cyan, lxm)}

  (* using other rules *)
  | "//"  {l_com_rule lexbuf}
  | "/*"  {m_com_rule lexbuf}
  | '"'   {let buffer = Buffer.create 20 in Buffer.add_char buffer '"'; str_rule buffer lexbuf}

  (* end of file, we just quit *)
  | eof {EOF}

  (* just continue *)
  | ws {main lexbuf}

  (* all the other characters are just red *)
  | _ as lxm {KEYWORD (red, string_of_char lxm)}

and l_com_rule = parse
  | '\n' {main lexbuf}
  | eof {EOF}
  | _ as lxm {l_com_rule lexbuf}

and m_com_rule = parse
  | "*/" {main lexbuf}
  | eof {EOF}
  | _ as lxm {m_com_rule lexbuf}

and str_rule buffer = parse
  | '"' {Buffer.add_char buffer '"'; KEYWORD (magenta, Buffer.contents buffer)}
  | "\\\"" as lxm {Buffer.add_string buffer lxm; str_rule buffer lexbuf}
  | eof {EOF}
  | _  as lxm {Buffer.add_char buffer lxm; str_rule buffer lexbuf}

{}

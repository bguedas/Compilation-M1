{
  let line_count = ref 0
}

let blank    = [' ' '\t' '\r']
let integer  = ['0'-'9']+
let number   = integer ('.' integer)?
let variable = 'x' integer

rule token = parse
  | "/*" {print_endline "Comment: begin"; comment lexbuf}
 
  | '\n'  {incr line_count; token lexbuf}
  | blank {token lexbuf} 

  | number   {print_endline "Number"; token lexbuf}
  | variable {print_endline "Variable"; token lexbuf}

  | eof {print_endline ("Number of lines: " ^ string_of_int !line_count)}
  | _   {print_endline "Unknown pattern"}
  
and comment = parse
  | "*/" {print_endline "Comment: end"; token lexbuf}
  | '\n' {incr line_count; comment lexbuf}
  | eof  {print_endline ("Number of lines: " ^ string_of_int !line_count)}
  | _    {comment lexbuf}
  
{
  let () =
    if Array.length Sys.argv < 2
      then print_endline "Please provide a file name" else
    let input_file = Sys.argv.(1) in
    if not (Sys.file_exists input_file)
      then print_endline "This file does not exist" else
    let lexbuf = Lexing.from_channel (open_in input_file) in
    token lexbuf
}

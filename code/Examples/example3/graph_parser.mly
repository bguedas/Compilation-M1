%{
open Functions

let io_list = ref [];; (* list of all the inputs and outputs *)
let names   = ref [];; (* list of all the module names *)

%}

%token EOF

%token MODULE
%token INPUTS
%token OUTPUTS

%token BEGIN_BLOCK
%token END_BLOCK
%token COLON
%token SEMICOLON
%token COMMA

%token <string> WORD

%start main
%type <Functions.t_modules> main

%%

main:
  def_modules EOF {!io_list,!names,$1}
;

def_modules:
    def_module {[$1]}
  | def_module def_modules {$1::$2}
;

def_module:
    MODULE WORD BEGIN_BLOCK def_inputs def_outputs END_BLOCK {names := list_add $2 !names; {name=$2; inputs=$4; outputs=$5}}
;

def_inputs:
    INPUTS COLON variable_list SEMICOLON {io_list := list_merge !io_list $3; $3}
;

def_outputs:
    OUTPUTS COLON variable_list SEMICOLON {io_list := list_merge !io_list $3; $3}
;

variable_list:
    WORD {[$1]}
  | WORD COMMA variable_list {$1::$3}
;

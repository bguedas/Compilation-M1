{
  open Graph_parser;;
}

let blank = [' ' '\t' '\r' '\n']
let word = ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '0'-'9']*

rule token = parse
  | "/*" {comment lexbuf}

  | "Module" {MODULE}
  | "in"     {INPUTS}
  | "out"    {OUTPUTS}

  | '{' {BEGIN_BLOCK}
  | '}' {END_BLOCK}
  | ':' {COLON}
  | ';' {SEMICOLON}
  | ',' {COMMA}

  | word as lxm {WORD lxm}
  | blank       {token lexbuf}
  | eof         {EOF}

and comment = parse
  | "*/" {token lexbuf}
  | _    {comment lexbuf}
  | eof  {EOF}

type t_module = {
  name    : string; 
  inputs  : string list;
  outputs : string list}

type t_modules = string list * string list * t_module list

(*-----------------*)
(* List operations *)
(*-----------------*)

(** Adds an element e to the list l if it does not already belong to l. *)
let rec list_add e l = match (e,l) with
    (a,[]) -> [a]
  | (a,b) when a = (List.hd b) -> b
  | (a,hd::tl) -> hd::(list_add a tl)

(** Removes the first occurrence of e from l. *)
let rec list_del e l = match (e,l) with
    (_,[]) -> []
  | (a,hd::tl) when a = hd -> tl
  | (a,hd::tl) -> hd::(list_del a tl)

(** Removes all the occurences of e from l. *)
let rec list_del_all e l = match (e,l) with
    (_,[]) -> []
  | (a,hd::tl) when a = hd -> list_del_all e tl
  | (a,hd::tl) -> hd::(list_del_all a tl)

(** Creates a list having all the elements of l1 which are not elements of l2. *)
let rec list_sub l1 l2 = match (l1,l2) with
    (a,[]) -> a
  | (a,hd::tl) -> list_sub (list_del hd a) tl

(** Merges two lists. *)
let rec list_merge l1 l2 = match (l1,l2) with
    ([],a) -> a
  | (hd::tl,a) -> list_merge tl (list_add hd a)

(*-----------------*)
(* Module printing *)
(*-----------------*)

let rec string_of_list = function
    [] -> ""
  | hd::tl -> hd^" "^(string_of_list tl)

let print_module m =
  print_endline (m.name^" :");
  print_endline ("\t"^"in : "^(string_of_list m.inputs));
  print_endline ("\t"^"out : "^(string_of_list m.outputs))

let print_modules (_,_,m) =
  List.iter print_module m

(*----------------*)
(* Graph printing *)
(*----------------*)

let print_io_list io_list =
  let lines l = print_endline ("\t"^l^" [shape=circle];") in
  List.iter lines io_list

let print_names n =
  let line l = print_endline ("\t"^l^" [shape=box];") in
  List.iter line n

let print_module_g m =
  let input e = print_endline ("\t"^e^" -> "^m.name) in
  let output s = print_endline ("\t"^m.name^" -> "^s) in
  List.iter input m.inputs;
  List.iter output m.outputs

let print_modules_g m =
  List.iter print_module_g m

let print_graph (io_list,names,m) =
  print_endline "digraph G {";
  print_io_list io_list;
  print_names names;
  print_modules_g m;
  print_endline "}"

{
  open Interpret_parser

  let get_num s =
    let stop = (String.length s) - 1 in
    let sub_s = String.sub s 1 stop in
    int_of_string sub_s
}

let f = 'F'
let integer = ['0'-'9']+
let number = integer ('.' integer)?
let variable = 'x' integer

rule token = parse
  | "/*" {comment lexbuf}

  | '+' {PLUS}
  | '-' {MINUS}
  | '*' {MUL}
  | '/' {DIV}
  | '^' {POW}
  | '=' {EQUAL}

  | 'F' {FUN}
  | '(' {BEGIN_PAR}
  | ')' {END_PAR}
  | ';' {SEMICOLON}

  | "cos" {COS}
  | "sin" {SIN}
  | "tan" {TAN}
  | "log" {LOG}
  | "exp" {EXP}

  | number as lxm   {NUM (float_of_string lxm)}
  | variable as lxm {VAR (get_num lxm)}

  | _   {token lexbuf}
  | eof {EOF}

and comment = parse
  | "*/" {token lexbuf}
  | _    {comment lexbuf}
  | eof  {EOF}

(**
 * An algebraic expression. It is represented by a tree where the leafs are
 * constants or variables, and the other nodes are operators. The different
 * variables are denoted by an index.
 *)
type t =
 | Const of float
 | Var of int       (* The index of the variable *)
 | Fun1 of (float -> float) * t
 | Fun2 of (float -> float -> float) * t * t

let rec evaluate = function
 | (_, Const x) -> x
 | (l, Var i)  -> List.nth  l (i-1)
 | (l, (Fun1 (f,x))) -> f (evaluate (l,x))
 | (l, (Fun2 (f,x,y))) -> f (evaluate (l,x)) (evaluate (l,y))

let evaluate_list f v = List.map (fun x -> evaluate (v,x)) f

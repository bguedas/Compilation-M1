%{
  open Expression
%}

%token EOF

%token PLUS
%token MINUS
%token MUL
%token DIV
%token POW
%token EQUAL

%token COS
%token SIN
%token TAN
%token LOG
%token EXP

%token FUN
%token BEGIN_PAR
%token END_PAR
%token SEMICOLON

%left PLUS MINUS
%left MUL DIV
%left UN
%right POW

%token <int> VAR
%token <float> NUM

%start main
%type <Expression.t list> main

%%

main:
  functions EOF {$1}
;

functions:
    func {[$1]}
  | func functions {$1::$2}
;

func:
    FUN EQUAL expression SEMICOLON {$3}
;

expression:
    VAR {Var $1}
  | NUM {Const $1}
  | MINUS expression %prec UN {Fun1 ((fun a -> -.a),$2)}
  | COS expression %prec UN {Fun1 (cos,$2)}
  | SIN expression %prec UN {Fun1 (sin,$2)}
  | TAN expression %prec UN {Fun1 (tan,$2)}
  | LOG expression %prec UN {Fun1 (log,$2)}
  | EXP expression %prec UN {Fun1 (exp,$2)}
  | expression PLUS expression {Fun2 ((+.),$1,$3)}
  | expression MINUS expression {Fun2 ((-.),$1,$3)}
  | expression MUL expression {Fun2 (( *.),$1,$3)}
  | expression DIV expression {Fun2 ((/.),$1,$3)}
  | expression POW expression {Fun2 (( **),$1,$3)}
  | BEGIN_PAR expression END_PAR {$2}
;

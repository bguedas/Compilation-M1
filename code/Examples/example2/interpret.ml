open Expression

(** Prints the result. *)
let print_evaluation = List.iter (fun x -> print_endline (string_of_float x))

(** Main *)
let () =
  if Array.length Sys.argv < 2
    then print_endline "Please, give a file name" else
  let input_file = Sys.argv.(1) in
  if not (Sys.file_exists input_file)
    then print_endline "This file does not exist" else
  let lexbuf = Lexing.from_channel (open_in input_file) in
  let f = Interpret_parser.main Interpret_lexer.token lexbuf in
  let r = evaluate_list f [5.;2.] in
  print_evaluation r

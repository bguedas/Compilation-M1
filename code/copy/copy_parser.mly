%token <char> CHAR
%token EOF
%start main
%type <string> main
%%

/*
 * A file is a list of characters ending with the end of file token. When
 * the end of the file is reached, we just print a new line.
 */
main:
   characters EOF {$1}
;

/*
 * A list of characters can be empty or composed of a list of characters
 * followed by a single character.
 *
 * This rule is left recursive because:
 *  - it is more efficient;
 *  - otherwise, the character input would be reversed. 
 */
characters:
    /* empty */ {" "}
  | characters CHAR {$1 ^ (String.make 1 $2)}
;

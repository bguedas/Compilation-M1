{
 open Copy_parser
}

rule main = parse
  | _ as c {CHAR c}
  | eof    {EOF}

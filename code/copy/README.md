Description
===========

A program that just copies the input file to an output file.

It is just the same program as cat, but it writes de output to a file instead
of standard output (terminal).

There is no need to use lex and yacc the write a cat program, but this
example shows how to put lex and yacc together to build a complete working
example.

Compile
=======

Just use the Makefile:

    make

You can delete the compiled files with the command:

    make clean

Run
===

You can run the program with a file as argument: 

    ./copy copy.mll



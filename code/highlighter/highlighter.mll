 {
  let red = "\x1b[31m"
  let green = "\x1b[32m"
  let yellow = "\x1b[33m"
  let blue = "\x1b[34m"
  let magenta = "\x1b[35m"
  let cyan = "\x1b[36m"

  let light_red = "\x1b[91m"
  let light_green = "\x1b[92m"
  let light_yellow = "\x1b[93m"
  let light_blue = "\x1b[94m"
  let light_magenta = "\x1b[95m"
  let light_cyan = "\x1b[96m"

  let reset () = print_string "\x1b[39;49m"

  (** Prints a string with the given color. *)
  let print_string_c clr str =
    print_string clr;
    print_string str;
    reset ();;

  (** Prints a character with the given color. *)
  let print_char_c clr chr =
    print_string clr;
    print_char chr;
    reset ();;
}

let integer  = ['0'-'9']+
let long = integer ('l' | 'L')
let float_or_double   = integer '.' integer ('D' | 'd' | 'F' | 'f')?
let number = integer | long | float_or_double
let p_type = "void" | "byte" | "int" | "short" | "long" | "float"
  | "double" | "bool" | "char"
let ident = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_' '.']* ['a'-'z' 'A'-'Z' '0'-'9' '_']
let pa_type = p_type? ('[' ']')? 

rule highlight = parse
  | "package"
  | "import"
  | "class"
  | "interface"
  | "enum"
  | "extends"
  | "implements"
  | "public"
  | "private"
  | "protected"
  | "static"
  | "abstract"
  | "final"
  | "new"
  | "thows"
  | "try"
  | "catch"
  | "finally"
  | "for"
  | "while"
  | "do"
  | "if"
  | "else"
  | "switch"
  | "case"
  | "return"
  | "instanceof" as lxm {print_string_c blue lxm; highlight lexbuf}
  | "null" {print_string_c light_red "null"; highlight lexbuf}

  (* character operators *)
  | '|' | '!'
  | '+' | '-' | '*' | '/'
  | '=' | '<' | '>' as lxm {print_char_c yellow lxm; highlight lexbuf}

  (* characters *)
  | ';' | ':'
  | '(' | ')'
  | '{' | '}' as lxm {print_char lxm; highlight lexbuf}

   (* primitive types including arrays *)
  | pa_type as lxm {print_string_c light_red lxm; highlight lexbuf}
 
  (* identifiers *)
  | ident as lxm {print_string lxm; highlight lexbuf}

  | number as lxm {print_string_c cyan lxm; highlight lexbuf}

  (* using other rules *)
  | "//"  {print_string_c green "//"; l_com_rule lexbuf}
  | "/*"  {print_string_c green "/*"; m_com_rule lexbuf}
  | "/**" {print_string_c light_blue "/*"; doc_rule lexbuf}
  | '"'   {print_char_c magenta '"'; str_rule lexbuf}

  (* end of file, we just quit *)
  | eof {}

  (* all the other characters are just printed *)
  | _ as lxm {print_char_c red lxm; highlight lexbuf}

and l_com_rule = parse
  | '\n' {print_char '\n'; highlight lexbuf}
  | eof {}
  | _ as lxm {print_char_c green lxm; l_com_rule lexbuf}

and m_com_rule = parse
  | "*/" {print_string_c green "*/"; highlight lexbuf}
  | eof {}
  | _ as lxm {print_char_c green lxm; m_com_rule lexbuf}

and doc_rule = parse
  | "*/" {print_string_c light_blue "*/"; highlight lexbuf}
  | eof {}
  | _ as lxm {print_char_c light_blue lxm; doc_rule lexbuf}

and str_rule = parse
  | '"' {print_char_c magenta '"'; highlight lexbuf}
  | "\\\"" as lxm {print_string lxm; str_rule lexbuf}
  | eof {}
  | _  as lxm {print_char_c magenta lxm; str_rule lexbuf}

{
  let () =
    if Array.length Sys.argv < 2
      then print_endline "Please provide a Java source file as agrument" else
      let input_file = Sys.argv.(1) in
        if not (Sys.file_exists input_file)
          then print_endline "This file does not exist" else
          let lexbuf = Lexing.from_channel (open_in input_file) in
          highlight lexbuf
}

package com.guedas.example;

/**
 * A useless demonstration class that contains some Java keywords.
 */
public class Demo {

    private static final Demo INSTANCE = new Demo();
    
    private int lineCount = 0;

    private Demo() {
      // Cannot be instantiated with new.
    }

    /*
     * Prints the given string to a new line with its corresponding line index. 
     */
    private void print(String str) {
      if (str == null) {
        throw new IllegalArgumentException("Cannot print null!");
      } else {
        System.out.println((lineCount++) + ": " + str);
      }
    } 

    public static void main(String[] args) {
        for (String arg : args) {
        	INSTANCE.print(arg);
        }
    }
}

{
 open Cat_parser
}

rule main = parse
  | _ as c {CHAR c}
  | eof    {EOF}

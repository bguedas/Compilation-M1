Description
===========

A program that just prints the input file to the standard output.

There is no need to use lex and yacc the write a cat program, but this
example shows how to put lex and yacc together to build a complete working
example.

Compile
=======

Just use the Makefile:

    make

You can delete the compiled files with the command:

    make clean

Run
===

You can run the program with a file as argument: 

    ./cat cat.mll


